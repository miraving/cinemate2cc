//
//  CCSecurity.h
//  cinemate
//
//  Created by miraving on 08.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObjectKey.h"
#import "CCUser.h"
#import "CCParserManager.h"


@interface CCSecurity : NSObject

+ (instancetype)instance;

- (void)addDelegate:(id)object;
- (void)removeDelegate:(id)object;

#pragma mark -

- (BOOL)auntheticationIsOK;
- (void)logout;

- (CCUser *)userProfile;

- (void)loginWithUserName:(NSString *)userLogin
              andPassword:(NSString *)pass
          completionBlock:(void (^)(BOOL flag))block;

#pragma mark -

- (void)getUpdateList;
- (void)getWatchList;
- (void)getMovieAndParseWithCCID:(NSNumber *)ccID;
- (void)getActorAndParseWithCCID:(NSNumber *)ccID;
- (void)searchWithTerm:(NSString *)term;

@end

@protocol CCSecurityDelegate <NSObject>

- (void)userProfileWasUpdated:(CCUser *)user;

@end