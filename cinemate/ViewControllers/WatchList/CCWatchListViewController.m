//
//  CCWatchListViewController.m
//  cinemate
//
//  Created by miraving on 17.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCWatchListViewController.h"
#import "CCMovieViewController.h"
#import "CCActorViewController.h"

@interface CCWatchListViewController ()
@end

@implementation CCWatchListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Dekegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo name];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        [cell setSelectionStyle:(UITableViewCellSelectionStyleNone)];
    }
    
    CDWatchList *item = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell.textLabel setText:[item name]];
    [cell.detailTextLabel setText:[item date]];
    if ([item.movie.isFullLoad boolValue] || [item.actor.isFullLoad boolValue])
    {
        cell.backgroundColor = [UIColor clearColor];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
    else
    {
        cell.backgroundColor = [UIColor lightGrayColor];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    CDWatchList *item = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if (item.movie != nil)
    {
        CCMovieViewController *movViewController = [[CCMovieViewController alloc] initWithMovieCCID:item.movie.ccID];
        [self.navigationController pushViewController:movViewController animated:YES];
    }
    else if (item.actor != nil)
    {
        CCActorViewController *actViewController = [[CCActorViewController alloc] initWithActorCCID:item.actor.ccID];
        [self.navigationController pushViewController:actViewController animated:YES];
    }
    else
    {
        CCLog(@"No load full profiler!");
    }
}

#pragma mark - NSFetchedResultsController Delegate

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController)
    {
        return _fetchedResultsController;
    }
    
    NSFetchedResultsController *result = nil;
    NSString *sectionKey = nil;
    NSFetchRequest *fetchRequest = nil;
    
    fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"CDWatchList"];
    NSSortDescriptor *sort0 = [NSSortDescriptor sortDescriptorWithKey:@"descriptionWatchList" ascending:NO];
    NSSortDescriptor *sort1 = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO];
    [fetchRequest setSortDescriptors:@[sort0, sort1]];
    sectionKey = @"descriptionWatchList";
    
    result = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                 managedObjectContext:[CCDatabaseManager instance].managedObjectContext
                                                   sectionNameKeyPath:sectionKey
                                                            cacheName:nil];
    
    [result performFetch:nil];
    result.delegate = self;
    
    _fetchedResultsController = result;
    return _fetchedResultsController;
}

#pragma mark - Private API

- (void)refresh
{
    [[CCSecurity instance] getWatchList];
    [self performSelector:@selector(stopRefresh) withObject:nil afterDelay:1];
}

@end
