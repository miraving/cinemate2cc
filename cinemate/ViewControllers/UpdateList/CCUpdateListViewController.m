//
//  CCUpdateListViewController.m
//  cinemate
//
//  Created by miraving on 17.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCUpdateListViewController.h"
#import "UIImageView+AFNetworking.h"


@interface CCUpdateListViewController ()

@end

@implementation CCUpdateListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    BOOL flag = [[sectionInfo name] boolValue];
    NSString *headerTitle = nil;
    if (flag)
        headerTitle = @"Новые раздачи";
    else
        headerTitle = @"Старые раздачи";
    
    return [self headerViewWithTitle:headerTitle];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    
    CDUpdateList *item = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *title = @"";
    NSString *imageURL = nil;
    if (item.movie)
    {
        title = [item.movie movieTitle];
    }
    else
    {
        title = [item.actor actorName];
        imageURL = item.actor.photo.medium;
    }
    [cell.textLabel setText:title];
    [cell.detailTextLabel setText:[NSString stringWithFormat:@"%@ [%@]", [item descriptionUpdateList], [item date]]];
    [cell.imageView setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"no_avatar"]];
    if ([[item isNew] boolValue])
    {
        cell.backgroundColor = CCRGBCOLOR(196,218,239);
    }
    else
    {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    [[CCHTTPClient instance] loadImagewithURL:[item.movie posterImageURL] success:^(id responseObject) {
        
        [cell.imageView setImage:responseObject];
    }];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CDUpdateList *item = [self.fetchedResultsController objectAtIndexPath:indexPath];
    item.isNew = [NSNumber numberWithInt:0];
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [[CCDatabaseManager instance] saveDatabase:nil];
}

#pragma mark - NSFetchedResultsController Delegate

- (NSFetchedResultsController *)fetchedResultsControllerBuild
{
    NSFetchedResultsController *result = nil;
    NSString *sectionKey = nil;
    NSFetchRequest *fetchRequest = nil;
    
    fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"CDUpdateList"];
    NSSortDescriptor *sort0 = [NSSortDescriptor sortDescriptorWithKey:@"isNew" ascending:NO];
    NSSortDescriptor *sort1 = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO];
    [fetchRequest setSortDescriptors:@[sort0, sort1]];
    sectionKey = @"isNew";
    
    result = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                 managedObjectContext:[CCDatabaseManager instance].managedObjectContext
                                                   sectionNameKeyPath:sectionKey
                                                            cacheName:nil];
    
    return result;
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController)
        return _fetchedResultsController;
    
    NSFetchedResultsController *aFRC = [self fetchedResultsControllerBuild];
    [aFRC performFetch:nil];
    aFRC.delegate = self;
    _fetchedResultsController = aFRC;
    
    return _fetchedResultsController;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView reloadData];
}

#pragma mark - Private API

- (void)refresh
{
    [[CCSecurity instance] getUpdateList];
    [self performSelector:@selector(stopRefresh) withObject:nil afterDelay:1];
}

- (UIView *)headerViewWithTitle:(NSString *)title
{
    CGRect headerFrame = self.tableView.frame;
    headerFrame.size.width = self.tableView.frame.size.width;
    headerFrame.size.height = 20;
    
    UILabel *label = [[UILabel alloc] initWithFrame:headerFrame];
    [label setBackgroundColor:[UIColor lightGrayColor]];
    [label setTextColor:[UIColor blackColor]];
    [label setText:title];

    return label;
}

@end
