//
//  CCCell.m
//  cinemate
//
//  Created by miraving on 26.03.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCCell.h"

@implementation CCCell

+ (NSString *)reuseIdentifer
{
    return [NSString stringWithFormat:@"%@_identifer", NSStringFromClass([self class])];
}

@end
