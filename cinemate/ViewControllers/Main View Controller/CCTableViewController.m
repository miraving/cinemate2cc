//
//  CCTableViewController.m
//  cinemate
//
//  Created by miraving on 09.04.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCTableViewController.h"

@interface CCTableViewController ()

@end

@implementation CCTableViewController

@synthesize tableView = _tableView;
@synthesize refreshController = _refreshController;
@synthesize fetchedResultsController = _fetchedResultsController;

#pragma mark - init

- (id)init
{
    self = [super init];
    if (self)
    {
        [self initialization];
    }
    return self;
}

- (void)initialization
{
    CGRect tableRect = [self.view bounds];
    if (self.navigationController)
    {
        tableRect.origin.y = 64;
    }
    self.tableView = [[UITableView alloc] initWithFrame:tableRect];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.view addSubview:self.tableView];
    
    self.refreshController = [[UIRefreshControl alloc] init];
    [self.refreshController addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshController];
    
    UIBarButtonItem *menu = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"sidebar_icon"]
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(menuOpen:)];
    [self.navigationItem setLeftBarButtonItem:menu];
}

#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [UITableViewCell new];
}

#pragma mark - NSFetchedResultsController Delegate

- (NSFetchedResultsController *)fetchedResultsController
{
    return nil;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
        {
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        }
        case NSFetchedResultsChangeDelete:
        {
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        }
        case NSFetchedResultsChangeUpdate:
        {
            [self.tableView reloadRowsAtIndexPaths:@[newIndexPath, indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeMove:
        {
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        }
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark - Private API

- (void)refresh
{
}

- (void)stopRefresh
{
    for (UIRefreshControl *rCtrl in self.tableView.subviews) {
        if ([rCtrl isKindOfClass:[UIRefreshControl class]]) {
            [rCtrl endRefreshing];
            continue;
        }
    }
}

@end
