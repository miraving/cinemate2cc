//
//  CCItemViewController.h
//  cinemate
//
//  Created by miraving on 3/17/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCItemViewController : UIViewController
@property (nonatomic, readwrite, strong) UIImageView *backgroundImage;

- (void)initialization;

@end
