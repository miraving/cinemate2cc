//
//  CCTableViewController.h
//  cinemate
//
//  Created by miraving on 09.04.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCViewController.h"
#import "CCActorViewController.h"
#import "CCMovieViewController.h"


@interface CCTableViewController : CCViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>
{
@protected
    UITableView *_tableView;
    UIRefreshControl *refreshController;
    NSFetchedResultsController *_fetchedResultsController;
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIRefreshControl *refreshController;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

- (void)initialization;
- (void)refresh;
- (void)stopRefresh;

@end
