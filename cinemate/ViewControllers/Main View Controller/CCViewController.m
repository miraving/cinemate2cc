//
//  CCViewController.m
//  cinemate
//
//  Created by miraving on 02.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCViewController.h"

@interface CCViewController () <CCSecurityDelegate>
@end

@implementation CCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self refresh];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [[CCDatabaseManager instance] saveDatabase:nil];
}

#pragma mark - Action

- (void)menuOpen:(id)sender
{
    [[[CCAppDelegate shared] sideViewController] toggleLeftSideMenuCompletion:nil];
}

- (void)refresh
{
}

- (void)stopRefresh
{
}

#pragma mark - CCSecurityDelegate

- (void)userProfileWasUpdated:(CCUser *)user
{
    [self refresh];
}

@end
