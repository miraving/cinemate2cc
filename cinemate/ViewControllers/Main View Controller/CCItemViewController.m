//
//  CCItemViewController.m
//  cinemate
//
//  Created by miraving on 3/17/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCItemViewController.h"

@interface CCItemViewController (protected)
@property (nonatomic, readwrite, strong) UIImageView *backgroundImage;
@end

@implementation CCItemViewController

- (void)initialization
{    
    self.backgroundImage = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [self.backgroundImage setContentMode:UIViewContentModeScaleAspectFill];
    [self.backgroundImage setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.backgroundImage];
}

@end
