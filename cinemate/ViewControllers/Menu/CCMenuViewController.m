//
//  DMTMMenuViewController.m
//  DMTM
//
//  Created by Vitalii Obertynskyi on 12/3/13.
//
//

#import "CCMenuViewController.h"
#import "CCAppDelegate.h"
#import "UIImageView+AFNetworking.h"
#import "UIView+Debug.h"

#define kHEADER_HIGHT       80.0
#define kDEF_TEXT_COLOR     CCRGBCOLOR(246,246,246)

@interface CCMenuViewController () <UITableViewDataSource, UITableViewDelegate, CCSecurityDelegate>
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, assign) SideMenuType lastSelectionMenu;
@end

@implementation CCMenuViewController

- (void)dealloc
{
    [[CCSecurity instance] removeDelegate:self];
}

- (id)init
{
   self = [super init];
   if (self)
   {
       [self initialization];
   }
   return self;
}

- (void)initialization
{
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.tableView setScrollEnabled:NO];
    [self.tableView setBackgroundColor:self.view.backgroundColor];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    [self.view setBackgroundColor:CCRGBCOLOR(42,43,47)];
    [self.view addSubview:self.tableView];
}

- (void)viewDidLoad
{
   [super viewDidLoad];
   self.lastSelectionMenu = kSideMenuTypeSearch;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
   return UIStatusBarStyleLightContent;
}

#pragma mark - CCSecurityDelegate

- (void)userProfileWasUpdated:(CCUser *)user
{
    [self.tableView reloadData];
}

#pragma mark - private API

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return kSideMenuMenuCount;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 220;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [UIView new];
    [header setBackgroundColor:[tableView backgroundColor]];
   
    CCUser *userInfo = [[CCSecurity instance] userProfile];
    
    if (userInfo)
    {
        NSString *userName = userInfo.userName;
        CGRect userPhotoFrame = CGRectMake(10, 30, kHEADER_HIGHT, kHEADER_HIGHT);
      
        UIImageView *headerImageView = [[UIImageView alloc] initWithFrame:userPhotoFrame];
        headerImageView.image = nil;
        [headerImageView cancelImageRequestOperation];
        
                [headerImageView setImage:[UIImage imageNamed:@"no_avatar"]];
        NSURL *imageURL = [userInfo bigAvatar];
        [[CCHTTPClient instance] loadImagewithURL:imageURL success:^(id responseObject)
        {
            if (responseObject)
            {
                [headerImageView setImage:responseObject];
            }
            else
            {
                NSLog(@"Avatar not load!");
            }
        }];
        [headerImageView.layer setCornerRadius:8];
        [headerImageView.layer setBorderColor:[[UIColor whiteColor] CGColor]];
        [headerImageView.layer setBorderWidth:2.f];
        [headerImageView setClipsToBounds:YES];
        [header addSubview:headerImageView];
      
        CGRect userNameLabelFrame = CGRectMake(userPhotoFrame.origin.x + kHEADER_HIGHT + 5,
                                             kHEADER_HIGHT / 2,
                                             320,
                                             (kHEADER_HIGHT) / 2);
        UILabel *headerUserNameLabel = [[UILabel alloc] initWithFrame:userNameLabelFrame];
        [headerUserNameLabel setText:userName];
        [headerUserNameLabel setFont:[UIFont fontWithName:@"Zapfino" size:18.f]];
        [headerUserNameLabel setBackgroundColor:[UIColor clearColor]];
        [headerUserNameLabel setTextColor:kDEF_TEXT_COLOR];
        [header addSubview:headerUserNameLabel];
   }
   return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
   if (cell == nil)
   {
       cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
       [cell setBackgroundColor:[UIColor clearColor]];
       [cell.textLabel setTextColor:kDEF_TEXT_COLOR];
       [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:18.f]];
       [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
   }
    
   NSString *menuTitle = nil;
   UIImage *menuImage = nil;
   
   switch (indexPath.row) {
      case kSideMenuTypeSearch:
       {
         menuTitle = @"Search";
         menuImage = [UIImage imageNamed:@"my_moments_icon"];
         break;
       }
      case kSideMenuTypeUpdateList:
       {
         menuTitle = @"Update List";
         menuImage = [UIImage imageNamed:@"search_people_icon"];
         break;
       }
//      case kSideMenuTypeWatchList:
//       {
//         menuTitle = @"Watch List";
//         menuImage = [UIImage imageNamed:@"people_in_my_day_icon"];
//         break;
//       }
      case kSideMenuTypeUserLogout:
       {
         menuTitle = @"Logout";
         menuImage = [UIImage imageNamed:@"learn_moves_icon"];
         break;
       }
   }
   
   [cell.textLabel setText:menuTitle];
   [cell.imageView setImage:menuImage];

   return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   self.lastSelectionMenu = indexPath.row;
   [[CCAppDelegate shared] selectSideMenuAtIndex:indexPath.row];
}

#pragma mark - Action

@end


