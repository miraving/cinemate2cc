//
//  DMTMMenuViewController.h
//  DMTM
//
//  Created by Vitalii Obertynskyi on 12/3/13.
//
//

#import <UIKit/UIKit.h>

typedef enum {
    kSideMenuTypeSearch = 0,
    kSideMenuTypeUpdateList,
//    kSideMenuTypeWatchList, // TODO:
    kSideMenuTypeUserLogout,
    kSideMenuMenuCount
} SideMenuType;

@interface CCMenuViewController : UIViewController

@end
