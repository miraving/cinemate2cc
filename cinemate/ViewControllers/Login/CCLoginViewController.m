//
//  CCLoginViewController.m
//  cinemate
//
//  Created by miraving on 02.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCLoginViewController.h"

@interface CCLoginViewController () <UITextFieldDelegate>
@property (strong, nonatomic) UITextField *login;
@property (strong, nonatomic) UITextField *pass;
@end

@implementation CCLoginViewController

- (id)init
{
    self = [super init];
    if (self)
    {
        [self initialization];
    }
    return self;
}

- (void)initialization
{
    [self.view setBackgroundColor:[UIColor lightGrayColor]];
    
    _login = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 300, 30)];
    _login.borderStyle = UITextBorderStyleBezel;
    _login.clearButtonMode = UITextFieldViewModeWhileEditing;
    _login.delegate = self;
    _login.placeholder = @"Enter login";
    _login.returnKeyType = UIReturnKeyNext;
    [self.view addSubview:self.login];
    
    UIImage *userIcon = [UIImage imageNamed:@"user-Icon"];
    UIImageView *userLeftIcon = [[UIImageView alloc] initWithImage:userIcon];
    [userLeftIcon setContentMode:UIViewContentModeScaleAspectFit];
    [userLeftIcon setFrame:CGRectMake(0, 0, 50, 25)];
    [self.login setLeftView:userLeftIcon];
    [self.login setLeftViewMode:UITextFieldViewModeAlways];
    
    _pass = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 300, 30)];
    _pass.borderStyle = UITextBorderStyleBezel;
    _pass.clearButtonMode = UITextFieldViewModeWhileEditing;
    _pass.placeholder = @"Your password";
    _pass.delegate = self;
    _pass.secureTextEntry = YES;
    _pass.returnKeyType = UIReturnKeyJoin;
    _pass.enablesReturnKeyAutomatically = YES;
    [self.view addSubview:self.pass];
    
    UIImage *passIcon = [UIImage imageNamed:@"pass-Icon"];
    UIImageView *passLeftIcon = [[UIImageView alloc] initWithImage:passIcon];
    [passLeftIcon setContentMode:UIViewContentModeScaleAspectFit];
    [passLeftIcon setFrame:CGRectMake(0, 0, 50, 25)];
    [self.pass setLeftView:passLeftIcon];
    [self.pass setLeftViewMode:UITextFieldViewModeAlways];
    
    CGPoint centr = [self.view center];
    centr.y -= 16;
    [self.login setCenter:centr];
    centr = [self.view center];
    centr.y += 16;
    [self.pass setCenter:centr];
    
#ifdef DEBUG
    self.login.text = @"miraving";
    self.pass.text = @"123581321";
    [self.pass becomeFirstResponder];
#endif
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)presentViewControllerWithParentViewController:(UIViewController *)parentViewController
{
    [parentViewController addChildViewController:self];
    self.view.frame = parentViewController.view.bounds;
    [parentViewController.view addSubview:self.view];
}

#pragma mark - Private API

- (void)auntetication
{
    if ([self.login.text length] == 0 || [self.pass.text length] == 0)
    {
        CCLog(@"First validation failed!");
        return;
    }
    
    [self showProgressAunthetication];
    [[CCSecurity instance] loginWithUserName:self.login.text
                                 andPassword:self.pass.text
                             completionBlock:^(BOOL flag) {
                                 
                                 if (flag)
                                 {
                                     [self.view setBackgroundColor:[UIColor greenColor]];
                                     [self dismiss:nil];
                                 }
                                 else
                                 {
                                     [self.view setBackgroundColor:[UIColor redColor]];
                                 }
                                 
                                 [self hideProgressAunthetication];
                             }];
}

- (void)dismiss:(id)sender
{
    [self.view removeFromSuperview];
    [self willMoveToParentViewController:nil];
    [self removeFromParentViewController];
}

- (void)showProgressAunthetication
{
    [self.login setEnabled:NO];
    [self.pass setEnabled:NO];
}

- (void)hideProgressAunthetication
{
    [self.login setEnabled:YES];
    [self.pass setEnabled:YES];
}

#pragma mark - UITextField delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.login])
    {
        [self.pass becomeFirstResponder];
        return NO;
    }
    [self auntetication];
    return YES;
}

@end
