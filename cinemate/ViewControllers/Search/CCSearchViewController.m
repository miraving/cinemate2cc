//
//  CCSearchViewController.m
//  cinemate
//
//  Created by miraving on 17.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCSearchViewController.h"

@interface CCSearchViewController () <UITextFieldDelegate, UIScrollViewDelegate>
@property (nonatomic, strong) UITextField *searchTextField;
@end

@implementation CCSearchViewController

- (void)initialization
{
    [super initialization];

    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    if (self.refreshController)
    {
        [self.refreshController removeFromSuperview];
        [self setRefreshController:nil];
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([self.searchTextField.text length] == 0)
    {
        return 0;
    }
    return [self.fetchedResultsController.sections count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"searchCell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"searchCell"];
    }
    CDMovie *item = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text =  [item movieTitle];
    cell.detailTextLabel.text = [item url];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id item = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ([item isKindOfClass:[CDActor class]])
    {
        CDActor *act = (CDActor *)item;
        CCActorViewController *actorViewController = [[CCActorViewController alloc] initWithActorCCID:act.ccID];
        [self.navigationController pushViewController:actorViewController animated:YES];
    }
    else if ([item isKindOfClass:[CDMovie class]])
    {
        CDMovie *mov = (CDMovie *)item;
        CCMovieViewController *movieViewController = [[CCMovieViewController alloc] initWithMovieCCID:mov.ccID];
        [self.navigationController pushViewController:movieViewController animated:YES];
    }
}

#pragma mark - NSFetchedResultsController Delegate

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController)
    {
        return _fetchedResultsController;
    }
    
    NSFetchedResultsController *result = nil;
    NSString *sectionKey = nil;
    NSFetchRequest *fetchRequest = nil;
    sectionKey = nil;
    
    fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"CDMovie"];
    NSSortDescriptor *sort0 = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:NO];
    [fetchRequest setSortDescriptors:@[sort0]];
    
    result = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                 managedObjectContext:[CCDatabaseManager instance].managedObjectContext
                                                   sectionNameKeyPath:sectionKey
                                                            cacheName:nil];
    
    [result performFetch:nil];
    result.delegate = self;
    
    _fetchedResultsController = result;
    return _fetchedResultsController;
}

#pragma mark - TextField delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [[CCDatabaseManager instance] saveDatabase:nil];
    if ([textField.text length] >= 2)
    {
        [[CCHTTPClient instance] cancelAllSearchTask];
        [[CCSecurity instance] searchWithTerm:textField.text];
        [textField resignFirstResponder];
    }
    self.fetchedResultsController.fetchRequest.predicate = [self predicate];
    [self.fetchedResultsController performFetch:nil];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string length] > 0)
    {
        self.fetchedResultsController.fetchRequest.predicate = [self predicate];
        [self.fetchedResultsController performFetch:nil];
        [self.tableView reloadData];
    }
    return YES;
}

#pragma mark - ScroolView Delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.searchTextField resignFirstResponder];
}

#pragma mark - Private API

- (NSPredicate *)predicate
{
    NSString *term = self.searchTextField.text;
    NSPredicate *p0 = [NSPredicate predicateWithFormat:@"title_russian CONTAINS[c] %@", term];
    NSPredicate *p1 = [NSPredicate predicateWithFormat:@"title_original CONTAINS[c] %@", term];
    NSPredicate *p2 = [NSPredicate predicateWithFormat:@"title_english CONTAINS[c] %@", term];
    
    return [NSCompoundPredicate orPredicateWithSubpredicates:@[p0, p1, p2]];
}

- (UIView *)searchView
{
    UIView *searchview = nil;
    if (self.searchTextField == nil)
    {
        searchview = [UIView new];
        [searchview setFrame:CGRectMake(0, 0, 320, 40)];
        CGRect searchRect = CGRectMake(4, 4, 312, 35);
        self.searchTextField = [[UITextField alloc] initWithFrame:searchRect];
        [self.searchTextField setDelegate:self];
        [self.searchTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [self.searchTextField setBorderStyle:UITextBorderStyleLine];
        [self.searchTextField setPlaceholder:@"Search"];
        [self.searchTextField setReturnKeyType:UIReturnKeySearch];
        [self.searchTextField setClearButtonMode:UITextFieldViewModeAlways];
        [searchview addSubview:self.searchTextField];
        
        UIImage *search = [UIImage imageNamed:@"search"];
        UIImageView *searchImage = [[UIImageView alloc] initWithImage:search];
        [searchImage setFrame:CGRectMake(0, 0, 22, 10)];
        [searchImage setContentMode:UIViewContentModeCenter];
        [self.searchTextField setLeftView:searchImage];
        [self.searchTextField setLeftViewMode:UITextFieldViewModeAlways];
    }
    else
    {
        searchview = self.searchTextField.superview;
    }
    
    return searchview;
}

@end





