//
//  CCMovieViewController.m
//  cinemate
//
//  Created by miraving on 3/17/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

typedef enum {
    kSectionMovieTypeTitle = 0,
    kSectionMovieTypePoster,
    kSectionMovieTypeDescription,
    kSectionMovieTypeRate,
    kSectionMovieTypeGenry,
    kSectionMovieTypeActor,
    kSectionMovieTypeDirector,
    kSectionMovieCount
} SectionMovieType;


#import "CCMovieViewController.h"
#import "CCMovieDescriptionCell.h"
#import "CCMovieHeaderCell.h"

@interface CCMovieViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSNumber *movieCCID;
@property (nonatomic, strong) CDMovie *movieProfile;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIImage *posterImage;
@end

@implementation CCMovieViewController

- (id)initWithMovieCCID:(NSNumber *)ccIDMovie
{
    self = [super init];
    if (self)
    {
        self.movieCCID = ccIDMovie;
        
        [self initialization];
    }
    return self;
}

- (void)initialization
{
    [super initialization];
    
    self.movieProfile = [CDMovie movieWithCCID:self.movieCCID];
    if ([self.movieProfile.isFullLoad boolValue] == NO)
    {
        [[CCSecurity instance] getMovieAndParseWithCCID:self.movieCCID];
    }
        
    [[CCHTTPClient instance] loadImagewithURL:[self.movieProfile posterImageURL] success:^(id responseObject)
    {
        UIImage *responseImage = responseObject;
        self.posterImage = responseImage;
        [self.backgroundImage setImage:[responseImage applyLightEffect]];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:kSectionMovieTypeTitle];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }];
    
    CGRect tableViewFrame = self.view.bounds;
    _tableView = [[UITableView alloc] initWithFrame:tableViewFrame style:UITableViewStyleGrouped];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setContentInset:UIEdgeInsetsMake(20, 0, 0, 0)];
    [self.view addSubview:self.tableView];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CCMovieDescriptionCell" bundle:nil] forCellReuseIdentifier:[CCMovieDescriptionCell reuseIdentifer]];
    [self.tableView registerNib:[UINib nibWithNibName:@"CCMovieHeaderCell" bundle:nil] forCellReuseIdentifier:[CCMovieHeaderCell reuseIdentifer]];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UItableview Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return kSectionMovieCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat result = 0.f;
    switch (section)
    {
        case kSectionMovieTypeDirector:
        case kSectionMovieTypeActor:
            result = 15.f;
            break;
        default:
            result = 0.f;
            break;
    }
    return result;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [self configureViewForHeaderInSection:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    
    switch (section)
    {
        case kSectionMovieTypeDescription:
        {
            NSString *str = self.movieProfile.descriptionMovie;
            return [CCMovieDescriptionCell cellHeightWithText:str];
        }
        case kSectionMovieTypeTitle:
        {
            return [CCMovieHeaderCell cellHeight];
        }
        case kSectionMovieTypePoster:
            return 0.f;
        default:
            return 40.f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifer = @"";
    switch (indexPath.section)
    {
        case kSectionMovieTypeDescription:
            identifer = [CCMovieDescriptionCell reuseIdentifer];
            break;
        case kSectionMovieTypeTitle:
            identifer = [CCMovieHeaderCell reuseIdentifer];
            break;
        default:
            identifer = @"cell";
            break;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifer];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    [self configureCell:cell withIndexPath:indexPath];
    
    return cell;
}

- (UIView *)configureViewForHeaderInSection:(NSInteger)section
{
    NSString *title = @"";
    UIView *result = nil;
    
    switch (section)
    {
        case kSectionMovieTypeTitle:
        case kSectionMovieTypePoster:
            break;
        case kSectionMovieTypeDescription:
            title = @"Description";
            break;
        case kSectionMovieTypeRate:
            title = @"Movie rate:";
            break;
        case kSectionMovieTypeGenry:
            title = @"Genry:";
            break;
        case kSectionMovieTypeActor:
            title = @"Actors:";
            break;
        case kSectionMovieTypeDirector:
            title = @"Director:";
            break;
    }
    
    if ([title length] > 0)
    {
        result = [UIView new];
        CGRect rect = self.view.bounds;
        rect.size.height = [self tableView:self.tableView heightForHeaderInSection:section];
        [result setFrame:rect];
        UILabel *label = [UILabel new];
        rect.origin.x += 10;
        rect.size.width -= 20;
        [label setFrame:rect];
        [label setText:title];
        [label setBackgroundColor:[UIColor clearColor]];
        [result addSubview:label];
    }

    return result;
}

- (void)configureCell:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    [cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
    [cell setBackgroundColor:[UIColor whiteColor]];
    [cell.imageView setImage:nil];
    
    switch (section)
    {
        case kSectionMovieTypeDescription:
        {
            CCMovieDescriptionCell *c = (CCMovieDescriptionCell *)cell;
            [c.descriptionMovie setText:self.movieProfile.descriptionMovie];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            break;
        }
        case kSectionMovieTypeTitle:
        {
            CCMovieHeaderCell *hc = (CCMovieHeaderCell *)cell;
            [hc.posterImageView setImage:self.posterImage];
            [hc.titleMovie setText:[self.movieProfile movieTitle]];
            [hc setBackgroundColor:[UIColor clearColor]];
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            break;
        }
        case kSectionMovieTypePoster:
        default:
        {
            [cell.textLabel setText:@"<none>"];
            break;
        }
    }
}

@end







