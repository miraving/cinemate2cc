//
//  CCMovieHeaderCell.h
//  cinemate
//
//  Created by Vitaliy Obertinskiy on 3/27/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCCell.h"

@interface CCMovieHeaderCell : CCCell

@property (weak, nonatomic) IBOutlet UILabel *titleMovie;
@property (weak, nonatomic) IBOutlet UIImageView *posterImageView;

+ (CGFloat)cellHeight;

@end
