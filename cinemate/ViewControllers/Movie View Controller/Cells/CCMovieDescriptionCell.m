//
//  CCMovieDescriptionCell.m
//  cinemate
//
//  Created by miraving on 26.03.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCMovieDescriptionCell.h"

@implementation CCMovieDescriptionCell

+ (CGFloat)cellHeightWithText:(NSString *)text
{
    CGFloat result = 0;
    
    if ([text length] > 0)
    {
        NSDictionary *att = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:14]};
        NSAttributedString *attStr = [[NSAttributedString alloc] initWithString:text attributes:att];
        UITextView *calculationView = [[UITextView alloc] init];
        [calculationView setAttributedText:attStr];
        CGSize size = [calculationView sizeThatFits:CGSizeMake(320, FLT_MAX)];
        result = size.height;
    }
    
    return result;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
