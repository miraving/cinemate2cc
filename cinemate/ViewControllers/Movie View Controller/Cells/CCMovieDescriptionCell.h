//
//  CCMovieDescriptionCell.h
//  cinemate
//
//  Created by miraving on 26.03.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCCell.h"

@interface CCMovieDescriptionCell : CCCell

@property (nonatomic, weak) IBOutlet UILabel *descriptionMovie;

+ (CGFloat)cellHeightWithText:(NSString *)text;

@end
