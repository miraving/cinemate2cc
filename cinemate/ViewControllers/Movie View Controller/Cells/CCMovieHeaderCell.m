//
//  CCMovieHeaderCell.m
//  cinemate
//
//  Created by Vitaliy Obertinskiy on 3/27/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCMovieHeaderCell.h"

@implementation CCMovieHeaderCell

+ (CGFloat)cellHeight
{
    return 300.f;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
