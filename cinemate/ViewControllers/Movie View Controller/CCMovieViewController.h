//
//  CCMovieViewController.h
//  cinemate
//
//  Created by miraving on 3/17/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCItemViewController.h"

@interface CCMovieViewController : CCItemViewController

- (id)initWithMovieCCID:(NSNumber *)ccIDMovie;

@end
