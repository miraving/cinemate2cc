//
//  CCCell.h
//  cinemate
//
//  Created by miraving on 26.03.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCCell : UITableViewCell

+ (NSString *)reuseIdentifer;

@end
