//
//  CCActorViewController.h
//  cinemate
//
//  Created by miraving on 3/17/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCItemViewController.h"

@interface CCActorViewController : CCItemViewController

- (id)initWithActorCCID:(NSNumber *)ccIDActor;

@end
