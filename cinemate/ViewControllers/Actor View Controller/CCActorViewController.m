//
//  CCActorViewController.m
//  cinemate
//
//  Created by miraving on 3/17/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCActorViewController.h"
#import "UIImageView+AFNetworking.h"

@interface CCActorViewController ()
@property (nonatomic, strong) NSNumber *actorCCID;
@property (nonatomic, strong) CDActor *actorProfile;
@end

@implementation CCActorViewController

- (id)initWithActorCCID:(NSNumber *)ccIDActor
{
    self = [super init];
    if (self)
    {
        self.actorCCID = ccIDActor;

        [self initialization];
    }
    return self;
}

- (void)initialization
{
    [super initialization];
    
    self.actorProfile = [CDActor actorWithCCID:self.actorCCID];
    if ([self.actorProfile.isFullLoad boolValue] == NO)
    {
        [[CCSecurity instance] getActorAndParseWithCCID:self.actorCCID];
    }
    else
    {
        NSURL *url = [NSURL URLWithString:self.actorProfile.photo.big];
        [self.backgroundImage setImageWithURL:url];
    
        [self setTitle:[self.actorProfile name_original]];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
