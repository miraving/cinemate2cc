//
//  CCAppDelegate.m
//  cinemate
//
//  Created by miraving on 02.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCAppDelegate.h"
#import "CCLoginViewController.h"
#import "CCMenuViewController.h"
#import "CCUpdateListViewController.h"
#import "CCWatchListViewController.h"
#import "CCSearchViewController.h"
#import "CCCache.h"
#import "AFNetworkActivityIndicatorManager.h"

@interface CCAppDelegate ()
@property (nonatomic, strong) NSMutableDictionary *stackViewController;
@end

@implementation CCAppDelegate

+ (id)shared
{
    return (CCAppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [UIWindow new];
    [self.window setFrame:[[UIScreen mainScreen] bounds]];
    
    [[CCDatabaseManager instance] openDatabase:nil];
    [AFNetworkActivityIndicatorManager.sharedManager setEnabled:YES];
    
    CCLoginViewController *loginViewController = [[CCLoginViewController alloc] init];;
    CCMenuViewController *menu = [CCMenuViewController new];
    [[CCSecurity instance] addDelegate:menu];
    
    self.sideViewController =
    [MFSideMenuContainerViewController containerWithCenterViewController:[self viewControllerForKey:kSideMenuTypeSearch]
                                                  leftMenuViewController:menu
                                                 rightMenuViewController:nil];
    [self.window setRootViewController:self.sideViewController];
    [self.window makeKeyAndVisible];
    [self.sideViewController setMenuState:MFSideMenuStateClosed];
    
    if (![[CCSecurity instance] auntheticationIsOK])
    {
        [loginViewController presentViewControllerWithParentViewController:self.sideViewController];
    }
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[CCDatabaseManager instance] saveDatabase:nil];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [[CCDatabaseManager instance] saveDatabase:nil];
}

#pragma mark - Private API
#pragma mark - Stack ViewControllers

- (UINavigationController *)navigationControllerForRootViewController:(UIViewController *)rootController
{
    UINavigationController *result = nil;
    if (rootController != nil)
    {
        result = [[UINavigationController alloc] initWithRootViewController:rootController];
        [result.navigationBar setTitleTextAttributes:[CCSettings navigationBarTextAttributes]];
    }
    return result;
}

- (UIViewController *)viewControllerForKey:(SideMenuType)menuKey
{
    if (self.stackViewController == nil)
        self.stackViewController = [NSMutableDictionary new];
    
    UIViewController *viewController = nil;
    
    NSString *key = [NSString stringWithFormat:@"viewController_%d", menuKey];
    viewController = [self.stackViewController objectForKey:key];
    if (viewController == nil)
    {
        switch (menuKey) {
            case kSideMenuTypeUpdateList:
            {
                CCViewController *viewCtrl = [[CCUpdateListViewController alloc] init];
                viewController = [self navigationControllerForRootViewController:viewCtrl];
                [viewCtrl setTitle:@"Update List"];
                break;
            }
            case kSideMenuTypeSearch:
            {
                UIViewController *viewCtrl = [[CCSearchViewController alloc] init];
                viewController = [self navigationControllerForRootViewController:viewCtrl];
                [viewCtrl setTitle:@"Search"];
                break;
            }
//            case kSideMenuTypeWatchList:
//            {
//                CCViewController *viewCtrl = [[CCWatchListViewController alloc] init];
//                viewController = [self navigationControllerForRootViewController:viewCtrl];
//                [viewCtrl setTitle:@"Watch List"];
//                break;
//            }
            case kSideMenuTypeUserLogout:
            {
                CCLoginViewController *loginvc = [[CCLoginViewController alloc] init];
                [loginvc presentViewControllerWithParentViewController:self.sideViewController];
                [[CCHTTPClient instance] cancelAllTask];
                [[CCCache instance] clearCache];
                [[CCDatabaseManager instance] clearDatabase];
                [[CCSecurity instance] logout];
                break;
            }
            default: // Login
                break;
        }
        if (viewController != nil)
        {
            [self.stackViewController setObject:viewController forKey:key];
        }
    }
    
    return viewController;
}

#pragma mark - Public API

- (void)selectSideMenuAtIndex:(SideMenuType)menuType {
    UIViewController *vc = [self viewControllerForKey:menuType];
    if (vc) {
        [self.sideViewController setCenterViewController:vc];
        [self.sideViewController setMenuState:MFSideMenuStateClosed];
    }
}

@end
