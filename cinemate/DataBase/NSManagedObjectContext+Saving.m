//
//  NSManagedObjectContext+Saving.m
//  DASDatabaseManager
//
//  Created by Alex Pawlowski on 11/01/12.
//  Copyright (c) 2012 Deloitte. All rights reserved.
//

#import "NSManagedObjectContext+Saving.h"

@implementation NSManagedObjectContext (Saving)

- (BOOL)saveWithError:(NSError **)error {
   if (![self hasChanges]) {
//      CCLog(@"NO CHANGES IN CONTEXT %@ - NOT SAVING", [self description]);
      return NO;
   }

   __block BOOL saved = NO;

   @try
   {
      [self performBlockAndWait: ^{
         saved = [self save:error];
      }

      ];
   }
   @catch (NSException *exception)
   {
      NSLog(@"Unable to perform save: %@", (id)[exception userInfo] ? : (id)[exception reason]);
   }

   return YES;
}

- (BOOL)saveInBackgroundWithError:(NSError **)error {
   [self performBlock: ^{
      [self saveNestedContextsWithError:error];
   }

   ];

   return YES;
}

- (BOOL)saveNestedContextsWithError:(NSError **)error {
   [self performBlock: ^{
      [self saveWithError:error];
      if (self.parentContext) {
         [[self parentContext] performBlock: ^{
               [[self parentContext] saveWithError:error];
            }

         ];
      }
   }

   ];

   return YES;
}

@end