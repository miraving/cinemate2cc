//
//  DASDatabaseManager.m
//  DASDatabaseManager
//
//  Created by Alex Pawlowski on 10/23/12.
//  Copyright (c) 2012 Infopulse. All rights reserved.
//

#import "CCDatabase.h"
#import "NSManagedObjectContext+Saving.h"

@interface CCDatabase () {
   NSManagedObjectModel *_managedObjectModel;
   NSManagedObjectContext *_rootManagedObjectContext;
   NSManagedObjectContext *_defaultManagedObjectContext;
   NSMutableDictionary *_managedObjectContexts;
}

- (void)threadWillExit:(NSNotification *)notification;

@end

@implementation CCDatabase

//+ (DASDatabaseManager *)instance {
//    static DASDatabaseManager *_instance = nil;
//
//    @synchronized (self) {
//        if (_instance == nil) {
//            _instance = [[self alloc] init];
//        }
//    }
//
//    return _instance;
//}

- (id)init {
   self = [super init];

   if (self) {
      _managedObjectContexts = [[NSMutableDictionary alloc] initWithCapacity:5];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(threadWillExit:)
                                                   name:NSThreadWillExitNotification
                                                 object:nil];
   }

   return self;
}

- (void)threadWillExit:(NSNotification *)notification {
   NSNumber *threadKey = [NSNumber numberWithInt:[NSThread currentThread].hash];

   if (threadKey && [threadKey isKindOfClass:[NSString class]]) {
      NSManagedObjectContext *threadContext = SAFE_ARC_RETAIN(SAFE_ARC_AUTORELEASE([_managedObjectContexts objectForKey:threadKey]));
      if (threadContext) {
         [_managedObjectContexts removeObjectForKey:threadKey];
      }
   }
}

- (NSURL *)modelURL {
   @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                  reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                userInfo:nil];
}

- (NSURL *)storeURL {
   @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                  reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                userInfo:nil];
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
   @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                  reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                userInfo:nil];
}

- (NSManagedObjectModel *)managedObjectModel {
   if (_managedObjectModel == nil) {
      _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:self.modelURL];
   }

   return _managedObjectModel;
}

- (NSManagedObjectContext *)managedObjectContext {
   if ([NSThread isMainThread] == YES) {
      return _defaultManagedObjectContext;
   }
   else {
      NSThread *thisThread = [NSThread currentThread];
      NSNumber *threadKey = [NSNumber numberWithInt:thisThread.hash];
      NSManagedObjectContext *threadContext = nil;

      @synchronized(_managedObjectContexts)
      {
         threadContext = [_managedObjectContexts objectForKey:threadKey];
      }

      if (!threadContext) {
         threadContext = SAFE_ARC_AUTORELEASE([[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType]);
         [threadContext setUndoManager:nil];
         [threadContext setParentContext:_defaultManagedObjectContext];
      }

      @synchronized(_managedObjectContexts)
      {
         (threadContext != nil) ? [_managedObjectContexts setObject:threadContext forKey:threadKey] : NSLog(@"Failed to get thread context");
      }

      return threadContext;
   }
}

- (BOOL)isDatabaseOpen {
   return (_defaultManagedObjectContext != nil);
}

- (BOOL)openDatabase:(NSError **)error {
   BOOL success = (_defaultManagedObjectContext != nil && _rootManagedObjectContext != nil);

   if (!success) {
      if (self.persistentStoreCoordinator) {
         _rootManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
         [_rootManagedObjectContext performBlockAndWait: ^{
            [_rootManagedObjectContext setPersistentStoreCoordinator:self.persistentStoreCoordinator];
         }

         ];
         [_rootManagedObjectContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];

         _defaultManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
         [_defaultManagedObjectContext setParentContext:_rootManagedObjectContext];

         success = YES;
      }

#ifdef DATABASE_PROTECTION
      NSDictionary *fileAttributes = [NSDictionary dictionaryWithObject:NSFileProtectionCompleteUnlessOpen
                                                                 forKey:NSFileProtectionKey];

      NSLog(@"fileAttributes %@", fileAttributes);

      if (![[NSFileManager defaultManager] setAttributes:fileAttributes
                                            ofItemAtPath:[self.storeURL path] error:error]) {
      }
#endif
   }

   return success;
}

- (BOOL)closeDatabase:(NSError **)error {
   BOOL success = [self saveDatabase:error];
   if (success) {
      SAFE_ARC_RELEASE(_defaultManagedObjectContext);
      _defaultManagedObjectContext = nil;
      SAFE_ARC_RELEASE(_persistentStoreCoordinator);
      _persistentStoreCoordinator = nil;
      @synchronized(_managedObjectContexts)
      {
         SAFE_ARC_RELEASE(_managedObjectContexts);
         _managedObjectContexts = nil;
      }
      SAFE_ARC_RELEASE(_rootManagedObjectContext);
      _rootManagedObjectContext = nil;
   }
   return success;
}

- (BOOL)saveDatabase:(NSError **)error {
   BOOL success = NO;
   NSError *_error = nil;

   if ([UIApplication sharedApplication].protectedDataAvailable) {
      NSManagedObjectContext *managedContext = self.managedObjectContext;
      if (managedContext && [managedContext hasChanges] && ![[NSThread currentThread] isCancelled]) {
         [managedContext saveWithError:&_error];
         if ([NSThread isMainThread]) {
            [_rootManagedObjectContext saveInBackgroundWithError:&_error];
//            NSLog(@"Persisting to disk...");
         }
      }
      if (_error == nil) {
         success = YES;
      }
   }

   return success;
}

- (BOOL)removeDatabase:(NSError **)error {
   [self closeDatabase:error];

   NSURL *storeURL = self.storeURL;
   NSFileManager *fileManager = [NSFileManager defaultManager];

   BOOL fileExists = [fileManager fileExistsAtPath:[storeURL path]];

   if (fileExists) {
      return [fileManager removeItemAtURL:storeURL error:error];
   }
   else {
      return YES;
   }
}

- (BOOL)resetContext {
   if (![NSThread isMainThread]) {
      NSNumber *threadKey = [NSNumber numberWithInt:[NSThread currentThread].hash];
      if (threadKey && [threadKey isKindOfClass:[NSString class]]) {
         @synchronized(_managedObjectContexts)
         {
            [_managedObjectContexts removeObjectForKey:threadKey];
         }
         return YES;
      }
   }
   return NO;
}

- (BOOL)deleteObjectByEntityName:(NSString *)entityName {
   BOOL success = NO;
   if (self.managedObjectContext) {
      NSFetchRequest *fetchRequest = SAFE_ARC_AUTORELEASE([NSFetchRequest new]);
      [fetchRequest setIncludesPropertyValues:NO];
      [fetchRequest setEntity:[NSEntityDescription entityForName:entityName
                                          inManagedObjectContext:self.managedObjectContext]];

      NSArray *objects = [self.managedObjectContext executeFetchRequest:fetchRequest error:NULL];

      success = (objects != nil);

      if (success) {
         for (NSManagedObject *object in objects)
         {
            [self.managedObjectContext deleteObject:object];
         }
      }
   }

   return success;
}

- (NSSet *)fetchObjectsForEntityName:(NSString *)newEntityName
                       withPredicate:(id)stringOrPredicate, ...{
   NSEntityDescription *entity = [NSEntityDescription entityForName:newEntityName
                                             inManagedObjectContext:self.managedObjectContext];

   NSFetchRequest *request = SAFE_ARC_AUTORELEASE([[NSFetchRequest alloc] init]);
   [request setEntity:entity];

   if (stringOrPredicate) {
      NSPredicate *predicate;
      if ([stringOrPredicate isKindOfClass:[NSString class]]) {
         va_list variadicArguments;
         va_start(variadicArguments, stringOrPredicate);
         predicate = [NSPredicate predicateWithFormat:stringOrPredicate arguments:variadicArguments];
         va_end(variadicArguments);
      }
      else {
         NSAssert2([stringOrPredicate isKindOfClass:[NSPredicate class]],
                   @"Second parameter passed to %s is of unexpected class %@", sel_getName(_cmd), [stringOrPredicate class]);
         predicate = (NSPredicate *)stringOrPredicate;
      }
      [request setPredicate:predicate];
   }

   NSError *error = nil;
   NSArray *results = [self.managedObjectContext executeFetchRequest:request error:&error];
   if (error != nil) {
      [NSException raise:NSGenericException format:@"%@", [error description]];
   }

   return [NSSet setWithArray:results];
}

- (void)dealloc {
   [[NSNotificationCenter defaultCenter] removeObserver:self];

   SAFE_ARC_RELEASE(_managedObjectModel);
   SAFE_ARC_RELEASE(_rootManagedObjectContext);
   SAFE_ARC_RELEASE(_defaultManagedObjectContext);
   SAFE_ARC_RELEASE(_persistentStoreCoordinator);
   [_managedObjectContexts removeAllObjects];
   SAFE_ARC_RELEASE(_managedObjectContexts);
   _managedObjectContexts = nil;

   SAFE_ARC_SUPER_DEALLOC();
}

@end