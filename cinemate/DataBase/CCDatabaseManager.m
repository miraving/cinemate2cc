//
//  MTMDatabaseManager.m
//  DMTM
//
//  Created by Sergii Movchan on 12/3/12.
//  Copyright (c) 2012 Sergii Tyshchenko. All rights reserved.
//

#import "CCDatabaseManager.h"

//#import "DMTMBrowserMoment+DMTMExtensions.h"
//#import "DMTMParticipant+DMTMExtensions.h"
//#import "DMTMMomentType+DMTMExtensions.h"


@interface CCDatabaseManager ()
- (void)deleteObjectForEntityname:(NSString *)entityName;
@end


@implementation CCDatabaseManager

+ (CCDatabaseManager *)instance
{
   static CCDatabaseManager *_instance = nil;

   @synchronized (self) {
      if (_instance == nil) {
         _instance = [[self alloc] init];
      }
   }

   return _instance;
}

- (NSURL *)modelURL
{
   return [[NSBundle mainBundle] URLForResource:@"CCModel" withExtension:@"momd"];
}

- (NSURL *)storeURL
{
   NSURL *directoryURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                                 inDomains:NSUserDomainMask] lastObject];
   // defaults
   NSString *databaseName = @"CCModel";
   return [directoryURL URLByAppendingPathComponent:databaseName];
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
   @synchronized (_persistentStoreCoordinator)
    {
      if (_persistentStoreCoordinator == nil)
      {
         NSManagedObjectModel *objectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:self.modelURL];

         if (objectModel)
         {
            NSPersistentStoreCoordinator *storeCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:objectModel];
            NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                                                                               [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
            NSError *error = nil;
            if ([storeCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:self.storeURL options:options error:&error]) {
               _persistentStoreCoordinator = storeCoordinator;
            }
            else
            {
               NSURL *fileURl = self.storeURL;

               if ([fileURl checkResourceIsReachableAndReturnError:&error])
               {
                  [[NSFileManager defaultManager] removeItemAtURL:fileURl error:&error];
               }

               if ([storeCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                  configuration:nil
                                                            URL:self.storeURL
                                                        options:options
                                                          error:&error])
               {
                  _persistentStoreCoordinator = storeCoordinator;
               }
            }
         }
      }

      return _persistentStoreCoordinator;
   }
}


#pragma mark - Insert Methods

- (CDActor *)insertActorWithProperties:(NSDictionary *)properties
{
    id value = [properties objectForKey:@"id"];
    
    CDActor *actor = nil;
    
    IfNumberValueNotNull(value)
    {
        actor = [CDActor actorWithCCID:value];
        if (!actor)
        {
            actor = [CDActor insertInManagedObjectContext:[CCDatabaseManager instance].managedObjectContext];
            actor.ccID = value;
        }
    }
    
    value = [properties objectForKey:@"name"];
    IfStringValueNotNull(value)
    {
        actor.name = value;
    }
    
    value = [properties objectForKey:@"name_original"];
    IfStringValueNotNull(value)
    {
        actor.name_original = value;
    }
    
    value = [properties objectForKey:@"url"];
    IfStringValueNotNull(value)
    {
        actor.url = value;
    }
    
    value = [properties objectForKey:@"photo"];
    IfDictionaryValueNotNull(value)
    {
        CDPhoto *photo = [CDPhoto photoWithCCID:actor.ccID];
        if (!photo)
        {
            photo = [CDPhoto insertInManagedObjectContext:[CCDatabaseManager instance].managedObjectContext];
            photo.ccID = actor.ccID;
        }
        
        value = [properties valueForKeyPath:@"photo.small.url"];
        IfStringValueNotNull(value)
        {
            photo.small = value;
        }
        
        value = [properties valueForKeyPath:@"photo.medium.url"];
        IfStringValueNotNull(value)
        {
            photo.medium = value;
        }
        
        value = [properties valueForKeyPath:@"photo.big.url"];
        IfStringValueNotNull(value)
        {
            photo.big = value;
        }
        
        actor.photo = photo;
    }
    NSError *err = nil;
    [self saveDatabase:&err];
    if (err)
        CCLog(@"Error: %@", err.localizedDescription);
    
    return actor;
}

- (CDMovie *)insertMovieWithProperties:(NSDictionary *)properties
{
    if (properties == nil)
        return nil;
    
    id value = [properties objectForKey:@"id"];
    CDMovie *movie = nil;
    
    IfNumberValueNotNull(value)
    {
        movie = [CDMovie movieWithCCID:value];
        if (!movie)
        {
            movie = [CDMovie insertInManagedObjectContext:[CCDatabaseManager instance].managedObjectContext];
            movie.ccID = value;
        }
    }
    
    value = [properties objectForKey:@"url"];
    IfStringValueNotNull(value)
    {
        movie.url = value;
    }
    
    value = [properties objectForKey:@"title_original"];
    IfStringValueNotNull(value)
    {
        movie.title_original = value;
    }
    
    value = [properties objectForKey:@"title_russian"];
    IfStringValueNotNull(value)
    {
        movie.title_russian = value;
    }
    
    value = [properties objectForKey:@"year"];
    IfNumberValueNotNull(value)
    {
        movie.year = [value stringValue];
    }
    
    value = [properties objectForKey:@"release_date_world"];
    IfStringValueNotNull(value)
    {
        NSDateFormatter *formater = [NSDateFormatter new];
        [formater setDateFormat:@"yyyy-MM-dd"];
        movie.release_date_world = [formater dateFromString:value];
    }
    
    value = [properties objectForKey:@"poster"];
    IfDictionaryValueNotNull(value)
    {
        NSDictionary *postersDict = value;
        CDPhoto *posters = [postersDict objectForKey:movie.ccID];
        if (!posters)
        {
            posters = [CDPhoto insertInManagedObjectContext:[CCDatabaseManager instance].managedObjectContext];
            posters.ccID = movie.ccID;
        }
        
        value = [postersDict valueForKeyPath:@"small.url"];
        IfStringValueNotNull(value)
        {
            posters.small = value;
        }
        
        value = [postersDict valueForKeyPath:@"big.url"];
        IfStringValueNotNull(value)
        {
            posters.big = value;
        }
        
        value = [postersDict valueForKeyPath:@"medium.url"];
        IfStringValueNotNull(value)
        {
            posters.medium = value;
        }
    }
    NSError *err = nil;
    [self saveDatabase:&err];
    if (err)
        CCLog(@"Error: %@", err.localizedDescription);
    
    return movie;
}

/*
- (DMTMBrowserMoment *)insertMomentWithProperties:(NSDictionary *)properties {
   id value = [properties objectForKey:MTM_MOMENT_ID];
   
   DMTMBrowserMoment*  moment = nil;
   
   IfNumberValueNotNull(value) {
      moment = [DMTMBrowserMoment browserMomentWithMomentID:(NSNumber *)value];
      
      if (!moment) {
         moment = [DMTMBrowserMoment insertInManagedObjectContext:[[CCDatabaseManager instance] managedObjectContext]];
         
         moment.momentID = (NSNumber *)value;
      }

      value = [properties objectForKey:MTM_MOMENT_IS_PRIVATE];
      IfNumberValueNotNull(value) {
         moment.isPrivate = (NSNumber *)value;
      }
      
      value = [properties objectForKey:MTM_MOMENT_LATITUDE];
      IfNumberValueNotNull(value) {
         moment.momentLatitude = (NSNumber *)value;
      }
      
      value = [properties objectForKey:MTM_MOMENT_LONGITUDE];
      IfNumberValueNotNull(value) {
         moment.momentLongitude = (NSNumber *)value;
      }
      
      value = [properties objectForKey:MTM_MOMENT_RECOGNIZER_NAME];
      IfStringValueNotNull(value) {
         moment.senderName = (NSString *)value;
      }
      
      value = [properties objectForKey:MTM_MOMENT_MAKER_NAME];
      IfStringValueNotNull(value) {
         moment.receiverName = (NSString *)value;
      }
      
      value = [properties objectForKey:MTM_MOMENT_RECOGNIZER_USER_NAME];
      IfStringValueNotNull(value) {
         moment.senderUsername = (NSString *)value;
      }
      
      value = [properties objectForKey:MTM_MOMENT_MAKER_USER_NAME];
      IfStringValueNotNull(value) {
         moment.receiverUsername = (NSString *)value;
      }
      
      
      value = [properties objectForKey:MTM_MOMENT_RECOGNIZER_PICTURE];
      IfStringValueNotNull(value) {
         moment.senderImageURL = (NSString *)value;
      }
      
      value = [properties objectForKey:MTM_MOMENT_MAKER_PIC_URL];
      IfStringValueNotNull(value) {
         moment.receiverImageURL = (NSString *)value;
      }
      
      value = [properties objectForKey:MTM_MOMENT_TEXT];
      IfStringValueNotNull(value) {
         moment.momentText = (NSString *)value;
      }
      
      value = [properties objectForKey:MTM_MOMENT_TYPE_ID];
      IfNumberValueNotNull(value) {
         moment.momentTypeID = (NSNumber *)value;
      }
      
      value = [properties objectForKey:MTM_MOMENT_TYPE_SHORT_DESC];
      IfStringValueNotNull(value) {
         moment.momentTypeName = (NSString *)value;
      }
      

      
      
      value = [properties objectForKey:MTM_MOMENT_CREATED_ON];
      IfStringValueNotNull(value) {
         int startPos = [value rangeOfString:@"("].location+1;
         int endPos = [value rangeOfString:@")"].location-5;
         NSRange range = NSMakeRange(startPos,endPos-startPos);
         
         int startPos2 = [value rangeOfString:@")"].location-5;
         int endPos2 = [value rangeOfString:@")"].location-1;
         NSRange range2 = NSMakeRange(startPos2,endPos2-startPos2);
         
         unsigned long long milliseconds = [[value substringWithRange:range] longLongValue];
         
         int gmt = [[value substringWithRange:range2] integerValue];
         
         NSTimeInterval interval = milliseconds/1000;         
         NSTimeInterval interval2 = (gmt/10.0)*3600.0;
         
         NSDate* date = [NSDate dateWithTimeIntervalSince1970:interval+interval2];
         
         moment.createdOn = date;
      }
//       @dynamic createdOn;
   }
   
   return moment;
   
}

- (DMTMParticipant *)insertParticipantWithProperties:(NSDictionary *)properties {
   
   DMTMParticipant *participant = nil;
   
   id value = [properties objectForKey:MTM_PARTICIPANT_MEMBERFIRM];
   IfStringValueNotNull(value) {
      NSString *country = (NSString *)value;
      if (![country isEqualToString:@"US"]) {
//         Parse only US account else return
         return participant;
      }
   }
   
   value = [properties objectForKey:MTM_PARTICIPANT_EMAILALIAS];
   
   IfStringValueNotNull(value){
   } else {
      value = [properties objectForKey:MTM_PARTICIPANT_EMAIL];
      value = [[value componentsSeparatedByString:@"@"] firstObject];
   }
   
   IfStringValueNotNull(value) {
      
      participant = [DMTMParticipant participantWithParticipantID:(NSString*)value];
      
      
      if (!participant) {
         participant = [DMTMParticipant insertInManagedObjectContext:[[CCDatabaseManager instance] managedObjectContext]];
         
      }
      
      participant.participantID = (NSString*)value;
      participant.emailAlias = (NSString*)value;
      
      
      
      value = [properties objectForKey:MTM_PARTICIPANT_DISPLAYNAME];
      
      IfStringValueNotNull(value) {
         participant.displayName = (NSString*)value;
      }
      
      value = [properties objectForKey:MTM_PARTICIPANT_FIRSTNAME];
      
      IfStringValueNotNull(value) {
         participant.firstName = (NSString*)value;
      }
      
      value = [properties objectForKey:MTM_PARTICIPANT_LASTNAME];
      
      IfStringValueNotNull(value) {
         participant.lastName = (NSString*)value;
      }
      
      value = [properties objectForKey:MTM_PARTICIPANT_OFFICECITY];
      
      IfStringValueNotNull(value) {
         participant.officeCity = (NSString*)value;
      }
      
      value = [properties objectForKey:MTM_PARTICIPANT_OFFICESTATE];
      
      IfStringValueNotNull(value) {
         participant.officeState = (NSString*)value;
      }
      
      value = [properties objectForKey:MTM_PARTICIPANT_MEMBERFIRM];
      
      IfStringValueNotNull(value) {
         participant.officeState = (NSString*)value;
      }
      
      value = [properties objectForKey:MTM_PARTICIPANT_PROFILEIMAGEURL];
      
      IfStringValueNotNull(value) {
         participant.profileURL = (NSString*)value;
      }
      
      value = [properties objectForKey:MTM_PARTICIPANT_PICTUREURL];
      
      IfStringValueNotNull(value) {
         participant.profileURL = (NSString *)value;
      }
   }
   
   return participant;
}


- (DMTMMomentType *)insertMomentTypeWithProperties:(NSDictionary *)properties {
   DMTMMomentType* mType = nil;
   
      id value = [properties objectForKey:MTM_MOMENTTYPE_MOVETYPEID];
   
   IfNumberValueNotNull(value) {
      
      mType = [DMTMMomentType momentTypeWithMomentTypeID:(NSNumber *)value];
      
      if (!mType) {
         mType = [DMTMMomentType insertInManagedObjectContext:[[CCDatabaseManager instance] managedObjectContext]];
         
         mType.momentTypeID = (NSNumber *)value;
      }
      
      
      value = [properties objectForKey:MTM_MOMENTTYPE_DISPLAYORDER];
      
      IfNumberValueNotNull(value) {
         mType.displayOrder = (NSNumber *)value;
      }
      
      value = [properties objectForKey:MTM_MOMENTTYPE_MOVETYPELONGDESC];
      
      IfStringValueNotNull(value) {
         mType.momentTypeDescription = (NSString *)value;
      }
      
      value = [properties objectForKey:MTM_MOMENTTYPE_MOVETYPESHORTDESC];
      
      IfStringValueNotNull(value) {
         mType.momentTypeName = (NSString *)value;
      }
      
      value = [properties objectForKey:MTM_MOMENTTYPE_ESSENCE];
      
      IfStringValueNotNull(value) {
         mType.momentTypeEssence = (NSString *)value;
      }

      
   }
   
   
   return mType;
}
*/

- (void)clearDatabase {
   NSArray *entityNames = [[self.managedObjectModel entities] valueForKey:@"name"];
   
   for (NSString* ent in entityNames) {      
      [self deleteObjectForEntityname:ent];
   }
   
   [self saveDatabase:nil];
}

- (void)deleteObjectForEntityname:(NSString *)entityName {
   if (![[NSThread currentThread] isMainThread]) { //Make sure that we are in main thread
      [self performSelectorOnMainThread:@selector(deleteObjectForEntityname:) withObject:entityName waitUntilDone:NO];
   }
   NSManagedObjectContext *context = self.managedObjectContext;
   NSFetchRequest *allObjects = [[NSFetchRequest alloc] init];
   [allObjects setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
   [allObjects setIncludesPropertyValues:NO]; //only fetch the managedObjectID
   
   NSError *error = nil;
   NSArray *arr = [context executeFetchRequest:allObjects error:&error];
   
   for (NSManagedObject *object in arr) {
      [context deleteObject:object];
   }
}

/*
#pragma mark - Events

- (DMTMEvent *)insertCalendarMeetingWithEvent:(EKEvent *)event {
   DMTMEvent *meeting = nil;
   
   if (event) {
      NSString *udid = nil;
      
      if ([event respondsToSelector:@selector(calendarItemIdentifier)]) {
         udid = [NSString stringWithFormat:@"%@%@",
                 event.calendarItemIdentifier,
                 [[[event startDate] description] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
      } else {
         udid = [NSString stringWithFormat:@"%@%@",
                 event.UUID,
                 [[[event startDate] description] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
      }
      
      NSLog(@"insertCalendarMeetingWithEvent UDID %@",udid);
      
      IfStringValueNotNull(udid) {
         meeting = [DMTMEvent meetingByUUID:udid];
      }
      
      if (!meeting) {
         meeting = [[DMTMEvent alloc] initWithEntity:[NSEntityDescription entityForName:@"DMTMEvent" inManagedObjectContext:self.managedObjectContext]
                      insertIntoManagedObjectContext:self.managedObjectContext];
      }
      
      
      meeting.uuid = udid;
      
      meeting.startDate = event.startDate;
      meeting.endDate = event.endDate;
      meeting.title = event.title;
      
      meeting.ownerEmail = [self emailFromDescription:event.organizer.description];
      
      NSMutableArray *paticipants = [NSMutableArray arrayWithCapacity:1];
      
      for (EKParticipant *user in event.attendees) {
         
         NSString *email = [self emailFromDescription:user.description];
         NSString *emailAlias = [[email componentsSeparatedByString:@"@"] firstObject];
         
         DMTMParticipant *paticipant = [DMTMParticipant participantWithParticipantEmailAlias:emailAlias];
         if (!paticipant) {
            paticipant = [[DMTMParticipant alloc] initWithEntity:[NSEntityDescription entityForName:@"DMTMParticipant" inManagedObjectContext:self.managedObjectContext]
                                  insertIntoManagedObjectContext:self.managedObjectContext];
            paticipant.emailAlias = emailAlias;
            paticipant.participantID = emailAlias;
            [[MTMHTTPClient instance] searchPeople:emailAlias success:^(AFHTTPRequestOperation *operation, id responseObject) {
               [[MTMParseManager sharedInstance] parseGetParticipantsWithFilename:responseObject complitionBlock:^(BOOL success) {
                  MTMLog(@"Parse participant file done.");
               }];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               MTMLog(@"");
            }];
         } else {
            [paticipants addObject:paticipant];
         }
      }
      
      meeting.attendee = [NSSet setWithArray:paticipants];
      
      NSLog(@"***********");
      NSLog(@"event %@", udid);
      NSLog(@"event.title %@",event.title);
      NSLog(@"event.startDate %@",event.startDate);
      NSLog(@"event.att.count %@", @([event.attendees count]));
      NSLog(@"***********");
   }
   
   return meeting;
}

- (NSString *)emailFromDescription:(NSString *)str {
   NSString *result = nil;
   
   NSArray *arr = [str componentsSeparatedByString:@";"];
   NSArray *emailArray = nil;
   
   for (NSString *comp in arr) {
      
      NSString *trimed = [comp stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
      
      if ([trimed hasPrefix:@"email"]) {
         emailArray = [trimed componentsSeparatedByString:@"="];
      }
   }
   
   if (emailArray && [emailArray count] == 2) {
      result = [[emailArray objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
   }
   
   return result;
}
*/
@end