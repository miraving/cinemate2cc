//
//  DASDatabaseManager.h
//  DASDatabaseManager
//
//  Created by Alex Pawlowski on 10/23/12.
//  Copyright (c) 2012 Infopulse. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "ConditionalARC.h"

/**
 *    @brief Singleton database manager class
 *    @details The DASDatabaseManager class performs convenient management of NSManagedObjectContext's in multi-threaded
 *    environment. Child contexts are encapsulated from direct access to NSPersistentStoreCoordinator through parent-child
 *    relationships.
 *    Saving contexts from the main thread triggers saving to the disk, while doing so from any other thread merges
 *    changes from that thread to main context.
 *    @copyright Deloitte Services LP 2012
 */
@interface CCDatabase : NSObject {
   @protected
   /**
    *    Protected iVar you can set in your subclass to store reference to the persistent store coordinator.
    */
   NSPersistentStoreCoordinator *_persistentStoreCoordinator;
}

@property (nonatomic, SAFE_ARC_PROP_RETAIN, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, SAFE_ARC_PROP_RETAIN, readonly) NSManagedObjectContext *managedObjectContext;
/**
 *     @details You shall override getter for this property in your subclass, depending on the type of the persistent store
 *     coordinator you'd like to use.
 */
@property (nonatomic, SAFE_ARC_PROP_RETAIN, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
/**
 *     @details You shall override getter for this property in your subclass, depending on the type of the model
 *     you'd like to use.
 */
@property (nonatomic, SAFE_ARC_PROP_WEAK_ASSIGN, readonly) NSURL *modelURL;
/**
 *     @details You shall override getter for this property in your subclass, depending on the type of the store
 *     you'd like to use.
 */
@property (nonatomic, SAFE_ARC_PROP_WEAK_ASSIGN, readonly) NSURL *storeURL;
@property (nonatomic, assign, readonly) BOOL isDatabaseOpen;
/**
 *    Returns shared instance of the singleton
 *    @returns An instance of the DASDatabaseManager
 */
//+ (DASDatabaseManager *)instance;

/**
 *    Opens the database if not already opened. Creates root saving context and main thread context
 *    @param error A pointer to pointer of NSError
 *    @returns BOOL indicating success of the operation
 */
- (BOOL)openDatabase:(NSError **)error;
/**
 *    Saves context of the current thread. If the thread it invoked from is main - also saves changes to the disk
 *    @param error A pointer to pointer of NSError
 *    @returns BOOL indicating success of the operation
 */
- (BOOL)saveDatabase:(NSError **)error;
/**
 *    Closes the database, destroys object contexts and persistent store coordinators
 *    @param error A pointer to pointer of NSError
 *    @returns BOOL indicating success of the operation
 */
- (BOOL)closeDatabase:(NSError **)error;
/**
 *    Closes the database and removes database file if appropriate
 *    @param error A pointer to pointer of NSError
 *    @returns BOOL indicating success of the operation
 */
- (BOOL)removeDatabase:(NSError **)error;
/**
 *    Destroys context of current thread
 *    @returns BOOL indicating success of the operation
 */
- (BOOL)resetContext;
/**
 *    Executes removing fetch request from the context on the thread this method is invoked
 *    @param entityName name of entity to remove
 *    @returns BOOL indicating success of the operation
 */
- (BOOL)deleteObjectByEntityName:(NSString *)entityName;
/**
 *    Executes fetch request with given entity and string or predicate on the context of current thread
 *    @param newEntityName name of entity to fetch
 *    @param stringOrPredicate string or predicate for fetch, and variadic list of arguments
 *    @returns NSSet with results of the fetch request
 */
- (NSSet *)fetchObjectsForEntityName:(NSString *)newEntityName withPredicate:(id)stringOrPredicate, ...;

@end