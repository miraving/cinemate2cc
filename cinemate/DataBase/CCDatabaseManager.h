//
//  MTMDatabaseManager.h
//  DMTM
//
//  Created by Sergii Movchan on 12/3/12.
//  Copyright (c) 2012 Sergii Tyshchenko. All rights reserved.
//

#import "CCDatabase.h"
#import "CCModel.h"

@class DMTMBrowserMoment;
@class DMTMParticipant;
@class DMTMMomentType;

@interface CCDatabaseManager : CCDatabase

+ (CCDatabaseManager *)instance;
- (void)clearDatabase;

//- (CDActor *)insertActorWithProperties:(NSDictionary *)properties;
//- (CDMovie *)insertMovieWithProperties:(NSDictionary *)properties;

@end