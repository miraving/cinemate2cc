//
//  CDWatchList+extension.h
//  cinemate
//
//  Created by miraving on 17.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CDWatchList.h"

@interface CDWatchList (extension)

+ (CDWatchList *)watchListItemWithCCID:(NSNumber *)ccID;

@end
