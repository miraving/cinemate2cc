//
//  CDMovie+extension.m
//  cinemate
//
//  Created by miraving on 10.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CDMovie+extension.h"

@implementation CDMovie (extension)

+ (CDMovie *)movieWithCCID:(NSNumber *)ccID
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([self class])];
    
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"ccID == %@", ccID]];
    NSError *error = nil;
    NSArray *results = [[CCDatabaseManager instance].managedObjectContext executeFetchRequest:fetchRequest
                                                                                        error:&error];
    if (results.count > 0)
    {
        return [results firstObject];
    }
    return nil;
}

- (NSString *)movieTitle
{
    IfStringValueNotNull(self.title_russian)
    {
        return self.title_russian;
    }
    
    IfStringValueNotNull(self.title_original)
    {
        return self.title_original;
    }
    
    return self.title_english;
}

- (NSURL *)posterImageURL
{
    NSString *result = nil;
    if ([self.poster.big length] > 0)
    {
        result = self.poster.big;
    }
    else if ([self.poster.medium length] > 0)
    {
        result = self.poster.medium;
    }
    else if ([self.poster.small length] > 0)
    {
        result = self.poster.small;
    }
    
    if ([result length] > 0)
    {
        return [NSURL URLWithString:result];
    }
    return nil;
}

@end
