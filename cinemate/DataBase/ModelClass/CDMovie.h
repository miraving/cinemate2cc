//
//  CDMovie.h
//  cinemate
//
//  Created by miraving on 3/27/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CDCheck.h"

@class CDActor, CDPhoto, CDUpdateList, CDWatchList;

@interface CDMovie : CDCheck

@property (nonatomic, retain) NSNumber * ccID;
@property (nonatomic, retain) NSString * descriptionMovie;
@property (nonatomic, retain) NSString * genre;
@property (nonatomic, retain) id imdb;
@property (nonatomic, retain) id kinopoisk;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDate * release_date_russia;
@property (nonatomic, retain) NSDate * release_date_world;
@property (nonatomic, retain) NSNumber * runtime;
@property (nonatomic, retain) NSString * title_english;
@property (nonatomic, retain) NSString * title_original;
@property (nonatomic, retain) NSString * title_russian;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * year;
@property (nonatomic, retain) NSSet *actors;
@property (nonatomic, retain) CDPhoto *poster;
@property (nonatomic, retain) NSSet *update;
@property (nonatomic, retain) NSSet *watch;
@end

@interface CDMovie (CoreDataGeneratedAccessors)

- (void)addActorsObject:(CDActor *)value;
- (void)removeActorsObject:(CDActor *)value;
- (void)addActors:(NSSet *)values;
- (void)removeActors:(NSSet *)values;

- (void)addUpdateObject:(CDUpdateList *)value;
- (void)removeUpdateObject:(CDUpdateList *)value;
- (void)addUpdate:(NSSet *)values;
- (void)removeUpdate:(NSSet *)values;

- (void)addWatchObject:(CDWatchList *)value;
- (void)removeWatchObject:(CDWatchList *)value;
- (void)addWatch:(NSSet *)values;
- (void)removeWatch:(NSSet *)values;

@end
