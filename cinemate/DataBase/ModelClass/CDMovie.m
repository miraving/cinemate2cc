//
//  CDMovie.m
//  cinemate
//
//  Created by miraving on 3/27/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CDMovie.h"
#import "CDActor.h"
#import "CDPhoto.h"
#import "CDUpdateList.h"
#import "CDWatchList.h"


@implementation CDMovie

@dynamic ccID;
@dynamic descriptionMovie;
@dynamic genre;
@dynamic imdb;
@dynamic kinopoisk;
@dynamic name;
@dynamic release_date_russia;
@dynamic release_date_world;
@dynamic runtime;
@dynamic title_english;
@dynamic title_original;
@dynamic title_russian;
@dynamic type;
@dynamic url;
@dynamic year;
@dynamic actors;
@dynamic poster;
@dynamic update;
@dynamic watch;

@end
