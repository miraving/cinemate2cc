//
//  CDMovie+extension.h
//  cinemate
//
//  Created by miraving on 10.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CDMovie.h"

@interface CDMovie (extension)

+ (CDMovie *)movieWithCCID:(NSNumber *)ccID;

- (NSString *)movieTitle;
- (NSURL *)posterImageURL;

@end
