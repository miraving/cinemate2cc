//
//  CDPhoto+extension.m
//  cinemate
//
//  Created by miraving on 04.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CDPhoto+extension.h"

@implementation CDPhoto (extension)

+ (CDPhoto *)photoWithCCID:(NSNumber *)ccID
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([self class])];
    
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"ccID == %@", ccID]];
    NSError *error = nil;
    NSArray *results = [[CCDatabaseManager instance].managedObjectContext executeFetchRequest:fetchRequest
                                                                                        error:&error];
    if (results.count > 0)
    {
        return [results firstObject];
    }
    
    return nil;
}

@end
