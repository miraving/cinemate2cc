//
//  CDActor+extension.h
//  cinemate
//
//  Created by miraving on 04.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CDActor.h"
#import "NSManagedObject+extension.h"

@interface CDActor (extension)

+ (CDActor *)actorWithCCID:(NSNumber *)ccID;

- (NSString *)actorName;

@end
