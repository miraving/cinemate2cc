//
//  CDUpdateList+extension.m
//  cinemate
//
//  Created by miraving on 17.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CDUpdateList+extension.h"

@implementation CDUpdateList (extension)

+ (CDUpdateList *)updateListItemWithDate:(NSString *)date
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([self class])];
    
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"date == %@", date]];
    NSError *error = nil;
    NSArray *results = [[CCDatabaseManager instance].managedObjectContext executeFetchRequest:fetchRequest
                                                                                        error:&error];
    if (results.count > 0)
    {
        return [results firstObject];
    }
    
    return nil;
}

@end
