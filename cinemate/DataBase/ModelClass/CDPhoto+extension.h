//
//  CDPhoto+extension.h
//  cinemate
//
//  Created by miraving on 04.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CDPhoto.h"

@interface CDPhoto (extension)

+ (CDPhoto *)photoWithCCID:(NSNumber *)ccID;

@end
