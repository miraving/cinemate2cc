//
//  CDActor.h
//  cinemate
//
//  Created by miraving on 24.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CDCheck.h"

@class CDMovie, CDPhoto, CDUpdateList, CDWatchList;

@interface CDActor : CDCheck

@property (nonatomic, retain) NSNumber * ccID;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * name_original;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSSet *movies;
@property (nonatomic, retain) CDPhoto *photo;
@property (nonatomic, retain) NSSet *update;
@property (nonatomic, retain) NSSet *watch;
@end

@interface CDActor (CoreDataGeneratedAccessors)

- (void)addMoviesObject:(CDMovie *)value;
- (void)removeMoviesObject:(CDMovie *)value;
- (void)addMovies:(NSSet *)values;
- (void)removeMovies:(NSSet *)values;

- (void)addUpdateObject:(CDUpdateList *)value;
- (void)removeUpdateObject:(CDUpdateList *)value;
- (void)addUpdate:(NSSet *)values;
- (void)removeUpdate:(NSSet *)values;

- (void)addWatchObject:(CDWatchList *)value;
- (void)removeWatchObject:(CDWatchList *)value;
- (void)addWatch:(NSSet *)values;
- (void)removeWatch:(NSSet *)values;

@end
