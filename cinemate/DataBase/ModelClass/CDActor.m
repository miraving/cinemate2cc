//
//  CDActor.m
//  cinemate
//
//  Created by miraving on 3/27/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CDActor.h"
#import "CDMovie.h"
#import "CDPhoto.h"
#import "CDUpdateList.h"
#import "CDWatchList.h"


@implementation CDActor

@dynamic ccID;
@dynamic name;
@dynamic name_original;
@dynamic url;
@dynamic movies;
@dynamic photo;
@dynamic update;
@dynamic watch;

@end
