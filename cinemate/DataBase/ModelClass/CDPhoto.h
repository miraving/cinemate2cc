//
//  CDPhoto.h
//  cinemate
//
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CDActor, CDMovie;

@interface CDPhoto : NSManagedObject

@property (nonatomic, retain) NSString * big;
@property (nonatomic, retain) NSNumber * ccID;
@property (nonatomic, retain) NSString * medium;
@property (nonatomic, retain) NSString * small;
@property (nonatomic, retain) CDActor *actorPhoto;
@property (nonatomic, retain) CDMovie *moviePoster;

@end
