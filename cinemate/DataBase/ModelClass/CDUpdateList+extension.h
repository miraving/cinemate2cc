//
//  CDUpdateList+extension.h
//  cinemate
//
//  Created by miraving on 17.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CDUpdateList.h"

@interface CDUpdateList (extension)

+ (CDUpdateList *)updateListItemWithDate:(NSString *)date;

@end
