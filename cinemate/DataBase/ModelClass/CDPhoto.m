//
//  CDPhoto.m
//  cinemate
//
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CDPhoto.h"
#import "CDActor.h"
#import "CDMovie.h"


@implementation CDPhoto

@dynamic big;
@dynamic ccID;
@dynamic medium;
@dynamic small;
@dynamic actorPhoto;
@dynamic moviePoster;

@end
