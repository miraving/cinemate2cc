//
//  CDUpdateList.h
//  cinemate
//
//  Created by miraving on 21.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CDActor, CDMovie;

@interface CDUpdateList : NSManagedObject

@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSString * descriptionUpdateList;
@property (nonatomic, retain) NSNumber * isNew;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) CDActor *actor;
@property (nonatomic, retain) CDMovie *movie;

@end
