//
//  CDSearch.h
//  cinemate
//
//  Created by miraving on 20.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CDSearch : NSManagedObject

@property (nonatomic, retain) NSDate * date;

@end
