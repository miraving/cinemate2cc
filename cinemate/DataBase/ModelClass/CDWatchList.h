//
//  CDWatchList.h
//  cinemate
//
//  Created by miraving on 24.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CDActor, CDMovie;

@interface CDWatchList : NSManagedObject

@property (nonatomic, retain) NSNumber * ccID;
@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSString * descriptionWatchList;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) CDMovie *movie;
@property (nonatomic, retain) CDActor *actor;

@end
