//
//  CDUpdateList.m
//  cinemate
//
//  Created by miraving on 21.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CDUpdateList.h"
#import "CDActor.h"
#import "CDMovie.h"


@implementation CDUpdateList

@dynamic date;
@dynamic descriptionUpdateList;
@dynamic isNew;
@dynamic url;
@dynamic actor;
@dynamic movie;

@end
