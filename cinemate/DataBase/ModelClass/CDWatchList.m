//
//  CDWatchList.m
//  cinemate
//
//  Created by miraving on 24.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CDWatchList.h"
#import "CDActor.h"
#import "CDMovie.h"


@implementation CDWatchList

@dynamic ccID;
@dynamic date;
@dynamic descriptionWatchList;
@dynamic name;
@dynamic url;
@dynamic movie;
@dynamic actor;

@end
