//
//  CDCheck.h
//  cinemate
//
//  Created by miraving on 3/27/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CDCheck : NSManagedObject

@property (nonatomic, retain) NSNumber * isFullLoad;
@property (nonatomic, retain) NSDate * dateLastLoad;

@end
