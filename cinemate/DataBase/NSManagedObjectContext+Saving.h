//
//  NSManagedObjectContext+Saving.h
//  DASDatabaseManager
//
//  Created by Alex Pawlowski on 11/01/12.
//  Copyright (c) 2012 Deloitte. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (Saving)

- (BOOL)saveWithError:(NSError **)error;
- (BOOL)saveInBackgroundWithError:(NSError **)error;
- (BOOL)saveNestedContextsWithError:(NSError **)error;

@end