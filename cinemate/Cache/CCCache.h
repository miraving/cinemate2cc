//
//  CCCache.h
//  cinemate
//
//  Created by miraving on 3/26/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCCache : NSObject

+ (id)instance;

- (void)clearCache;
- (BOOL)objectExistAtURL:(NSURL *)url;
- (UIImage *)imageForURL:(NSURL *)url;
- (BOOL)saveImage:(UIImage *)img forURL:(NSURL *)url;

@end
