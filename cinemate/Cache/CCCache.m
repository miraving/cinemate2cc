//
//  CCCache.m
//  cinemate
//
//  Created by miraving on 3/26/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCCache.h"

@interface CCCache ()
@property (nonatomic, strong) NSFileManager *fileManager;
@end

@implementation CCCache

+ (id)instance
{
    static CCCache *inst = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        inst = [CCCache new];
        inst.fileManager = [NSFileManager defaultManager];
        
        BOOL flag = YES;
        if (![inst.fileManager fileExistsAtPath:[inst cachePath] isDirectory:&flag])
        {
            [inst.fileManager createDirectoryAtPath:[inst cachePath]
                        withIntermediateDirectories:YES
                                         attributes:nil
                                              error:nil];
        }
    });
    return inst;
}

- (void)clearCache
{
    BOOL flag = YES;
    if ([self.fileManager fileExistsAtPath:[self cachePath] isDirectory:&flag])
    {
        NSError *error = nil;
        [self.fileManager removeItemAtPath:[self cachePath] error:&error];
        if (error)
        {
            CCLog(@"Error remove cache directory: %@", error.localizedDescription);
        }
    }
}

- (BOOL)objectExistAtURL:(NSURL *)url
{
    return [self objectExistAtStringURL:[url absoluteString]];
}

- (BOOL)objectExistAtStringURL:(NSString *)strUrl
{
    NSString *fileName = [self fileNameWithStringURL:strUrl];
    BOOL flag = NO;
    if ([self.fileManager fileExistsAtPath:[self cachePathWithComponent:fileName] isDirectory:&flag])
    {
        return YES;
    }
    return NO;
}

- (UIImage *)imageForURL:(NSURL *)url
{
    NSString *filePath = [self cachePathWithComponent:[self fileNameWithStringURL:[url absoluteString]]];
    UIImage *img = [UIImage imageWithContentsOfFile:filePath];
    return img;
}

- (BOOL)saveImage:(UIImage *)img forURL:(NSURL *)url
{
    NSData *data = UIImagePNGRepresentation(img);
    NSString *filePath = [self cachePathWithComponent:[self fileNameWithStringURL:[url absoluteString]]];
    NSError *error = nil;
    
    [data writeToFile:filePath options:NSDataWritingFileProtectionComplete error:&error];

    if (error == nil)
        return NO;
    
    return YES;
}

#pragma mark - Private API

- (NSString *)fileNameWithStringURL:(NSString *)str
{
    return [NSString stringWithFormat:@"%d", [str hash]];
}

- (NSString *)cachePath
{
    NSString *result = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    result = [result stringByAppendingPathComponent:@"cc_image"];
    return result;
}

- (NSString *)cachePathWithComponent:(NSString *)component
{
    return [[self cachePath] stringByAppendingPathComponent:component];
}

@end
