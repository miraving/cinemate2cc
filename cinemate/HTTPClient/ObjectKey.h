
#define kOBJECT_KEY_PASSKEY         @"passkey"
#define kUSER_NAME                  @"username"
#define kUSER_REPUTATION            @"reputation"
#define kUSER_AVATAR_BIG_PATH       @"avatar.big.url"
#define kUSER_AVATAR_SMALL_PATH     @"avatar.small.url"