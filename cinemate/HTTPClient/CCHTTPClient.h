//
//  CCHTTPClient.h
//  cinemate
//
//  Created by miraving on 08.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface CCHTTPClient : AFHTTPSessionManager

+ (instancetype)instance;

- (void)cancelAllTask;
- (void)cancelAllSearchTask;

- (NSURLSessionDataTask *)authenticationUser:(NSString *)user
                                    password:(NSString *)password
                                  completion:(void (^)(NSString *results, NSError *error))completion;

- (NSURLSessionDataTask *)getUserProfileWithPassKey:(NSString *)key
                                         completion:(void (^)(NSDictionary *results, NSError *error))completion;

- (NSURLSessionDataTask *)getWatchListWithPassKey:(NSString *)key
                                       completion:(void (^)(NSDictionary *results, NSError *error))completion;

- (NSURLSessionDataTask *)getUpdateListWithPassKey:(NSString *)key
                                       completion:(void (^)(NSDictionary *results, NSError *error))completion;

- (NSURLSessionDataTask *)getActorWithActorCCID:(NSString *)ccID
                                     completion:(void (^)(NSDictionary *results, NSError *error))completion;

- (NSURLSessionDataTask *)getMovieWithMovieCCID:(NSString *)ccID
                                     completion:(void (^)(NSDictionary *results, NSError *error))completion;

- (NSURLSessionDataTask *)searchMovieWithTerm:(NSString *)term
                                   completion:(void (^)(NSDictionary *results, NSError *error))completion;

- (NSURLSessionDataTask *)searchActorWithTerm:(NSString *)term
                                   completion:(void (^)(NSDictionary *results, NSError *error))completion;

- (NSURLSessionDataTask *)statisticsSinemateCompletion:(void (^)(NSDictionary *results, NSError *error))completion;

- (void)loadImagewithURL:(NSURL *)url success:(void (^)(id responseObject))success; //__attribute__ ((deprecated));

@end
