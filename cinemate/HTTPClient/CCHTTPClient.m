//
//  CCHTTPClient.m
//  cinemate
//
//  Created by miraving on 08.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCHTTPClient.h"
#import "AFNetworking.h"
#import "CCParserManager.h"
#import "CCCache.h"

#define kAPI_KEY    @"230fc394eee763115c872a253c4319c175ae876e"
#define kFORMAT     @"json"

@implementation CCHTTPClient

+ (instancetype)instance
{
    static CCHTTPClient *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *baseURL = [NSURL URLWithString:@"http://api.cinemate.cc/"];
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLCache *cache = [[NSURLCache alloc] initWithMemoryCapacity:50 * 1024 * 1024
                                                          diskCapacity:500 * 1024 * 1024
                                                              diskPath:nil];
        [config setURLCache:cache];
        config.HTTPMaximumConnectionsPerHost = 1;
        instance = [[CCHTTPClient alloc] initWithBaseURL:baseURL sessionConfiguration:config];
        instance.responseSerializer = [AFJSONResponseSerializer serializer];
        instance.requestSerializer = [AFJSONRequestSerializer serializer];
    });
    return instance;
}

- (void)cancelAllTask
{
    for (NSURLSessionDataTask *task in self.operationQueue.operations)
    {
        if (task.state != NSURLSessionTaskStateCompleted)
        {
            [task cancel];
        }
    }
}

- (void)cancelAllSearchTask
{
    for (NSURLSessionTask *task in self.operationQueue.operations)
    {
        NSString *searchPath = [task.originalRequest.URL path];
        if ([searchPath isEqualToString:@"movie.search"] || [searchPath isEqualToString:@"person.search"])
        {
            [task cancel];
        }
    }
}

#pragma mark - aunthetication

- (NSURLSessionDataTask *)authenticationUser:(NSString *)user
                                    password:(NSString *)password
                                  completion:(void (^)(NSString *results, NSError *error))completion
{
    NSDictionary *parameters = @{@"username": user,
                                 @"password": password,
                                 @"format": kFORMAT};
    
   NSURLSessionDataTask *task = [self GET:@"account.auth"
                               parameters:parameters
                                  success:^(NSURLSessionDataTask *task, id responseObject) {
                                    
                                      NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                      if (httpResponse.statusCode == 200)
                                      {
                                          completion(responseObject[@"passkey"], nil);
                                      }
                                      else
                                      {
                                          completion(nil, nil);
                                          CCLog(@"Received: %@", responseObject);
                                          CCLog(@"Received HTTP %d", httpResponse.statusCode);
                                      }
                                  } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                      completion(nil, error);
                                  }];
    return task;
}

#pragma mark - profile

- (NSURLSessionDataTask *)getUserProfileWithPassKey:(NSString *)key
                                         completion:(void (^)(NSDictionary *results, NSError *error))completion
{
    NSDictionary *parameters = @{@"passkey": key,
                                 @"format": kFORMAT};
    NSURLSessionDataTask *task = [self GET:@"account.profile"
                                parameters:parameters
                                   success:^(NSURLSessionDataTask *task, id responseObject) {
                                       NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                       if (httpResponse.statusCode == 200)
                                       {
                                           completion(responseObject, nil);
                                       }
                                       else
                                       {
                                           completion(nil, nil);
                                           CCLog(@"Received: %@", responseObject);
                                           CCLog(@"Received HTTP %d", httpResponse.statusCode);
                                       }
                                   } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                       completion(nil, error);
                                   }];
    return task;
}

- (NSURLSessionDataTask *)getWatchListWithPassKey:(NSString *)key
                                       completion:(void (^)(NSDictionary *results, NSError *error))completion
{
    NSDictionary *parameters = @{@"passkey": key,
                                 @"format": kFORMAT};
    NSURLSessionDataTask *task = [self GET:@"account.watchlist"
                                parameters:parameters
                                   success:^(NSURLSessionDataTask *task, id responseObject) {
                                       NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                       if (httpResponse.statusCode == 200)
                                       {
                                           completion(responseObject, nil);
                                       }
                                       else
                                       {
                                           completion(nil, nil);
                                           CCLog(@"Received: %@", responseObject);
                                           CCLog(@"Received HTTP %d", httpResponse.statusCode);
                                       }
                                   } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                       completion(nil, error);
                                   }];
    return task;
}

- (NSURLSessionDataTask *)getUpdateListWithPassKey:(NSString *)key
                                        completion:(void (^)(NSDictionary *results, NSError *error))completion
{
    NSDictionary *parameters = @{@"passkey": key,
                                 @"newonly": @"1",
                                 @"format": kFORMAT};
    NSURLSessionDataTask *task = [self GET:@"account.updatelist"
                                parameters:parameters
                                   success:^(NSURLSessionDataTask *task, id responseObject) {
                                       
                                       NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                       if (httpResponse.statusCode == 200)
                                       {
                                           completion(responseObject, nil);
                                       }
                                       else
                                       {
                                           completion(nil, nil);
                                           CCLog(@"Received: %@", responseObject);
                                           CCLog(@"Received HTTP %d", httpResponse.statusCode);
                                       }
                                       
                                   } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                       completion(nil, error);
                                   }];
    return task;
}

- (NSURLSessionDataTask *)getActorWithActorCCID:(NSString *)ccID
                                     completion:(void (^)(NSDictionary *results, NSError *error))completion
{
    NSDictionary *parameters = @{@"id": ccID,
                                 @"apikey": kAPI_KEY,
                                 @"format": kFORMAT};

    NSURLSessionDataTask *task = [self GET:@"person"
                                parameters:parameters
                                   success:^(NSURLSessionDataTask *task, id responseObject) {
                                       
                                       NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                       if (httpResponse.statusCode == 200)
                                       {
                                           completion(responseObject, nil);
                                       }
                                       else
                                       {
                                           completion(nil, nil);
                                           CCLog(@"Received: %@", responseObject);
                                           CCLog(@"Received HTTP %d", httpResponse.statusCode);
                                       }
                                   } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                       completion(nil, error);
                                   }];
    return task;
}

- (NSURLSessionDataTask *)getMovieWithMovieCCID:(NSString *)ccID
                                     completion:(void (^)(NSDictionary *results, NSError *error))completion
{
    NSDictionary *parameters = @{@"id": ccID,
                                 @"apikey": kAPI_KEY,
                                 @"format": kFORMAT};
    
    NSURLSessionDataTask *task = [self GET:@"movie"
                                parameters:parameters
                                   success:^(NSURLSessionDataTask *task, id responseObject) {
                                       
                                       NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                       if (httpResponse.statusCode == 200)
                                       {
                                           completion(responseObject, nil);
                                       }
                                       else
                                       {
                                           completion(nil, nil);
                                           CCLog(@"Received: %@", responseObject);
                                           CCLog(@"Received HTTP %d", httpResponse.statusCode);
                                       }
                                   } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                       completion(nil, error);
                                   }];
    [task setTaskDescription:[parameters description]];
    return task;
}

- (NSURLSessionDataTask *)searchMovieWithTerm:(NSString *)term
                                    completion:(void (^)(NSDictionary *results, NSError *error))completion
{
    NSDictionary *parameters = @{@"term": [term stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                                 @"apikey": kAPI_KEY,
                                 @"format": kFORMAT};
    NSURLSessionDataTask *task = [self GET:@"movie.search"
                                parameters:parameters
                                   success:^(NSURLSessionDataTask *task, id responseObject) {
                                       NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                       if (httpResponse.statusCode == 200)
                                       {
                                           completion(responseObject, nil);
                                       }
                                       else
                                       {
                                           completion(nil, nil);
                                           CCLog(@"Received: %@", responseObject);
                                           CCLog(@"Received HTTP %d", httpResponse.statusCode);
                                       }
                                   } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                       completion(nil, error);
                                   }];
    return task;
}

- (NSURLSessionDataTask *)searchActorWithTerm:(NSString *)term
                                   completion:(void (^)(NSDictionary *results, NSError *error))completion
{
    NSDictionary *parameters = @{@"term": [term stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                                 @"apikey": kAPI_KEY,
                                 @"format": kFORMAT};
    NSURLSessionDataTask *task = [self GET:@"person.search"
                                parameters:parameters
                                   success:^(NSURLSessionDataTask *task, id responseObject) {
                                       NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                       if (httpResponse.statusCode == 200)
                                       {
                                           completion(responseObject, nil);
                                       }
                                       else
                                       {
                                           completion(nil, nil);
                                           CCLog(@"Received: %@", responseObject);
                                           CCLog(@"Received HTTP %d", httpResponse.statusCode);
                                       }
                                   } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                       completion(nil, error);
                                   }];
    return task;
}

- (NSURLSessionDataTask *)statisticsSinemateCompletion:(void (^)(NSDictionary *results, NSError *error))completion
{
    NSDictionary *parameters = @{@"format": kFORMAT};
    
    NSURLSessionDataTask *task = [self GET:@"stats.new"
                                parameters:parameters
                                   success:^(NSURLSessionDataTask *task, id responseObject) {
                                       NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                       if (httpResponse.statusCode == 200)
                                       {
                                           completion(responseObject, nil);
                                       }
                                       else
                                       {
                                           completion(nil, nil);
                                           CCLog(@"Received: %@", responseObject);
                                           CCLog(@"Received HTTP %d", httpResponse.statusCode);
                                       }
                                   } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                       completion(nil, error);
                                   }];
    return task;
}

- (void)loadImagewithURL:(NSURL *)url success:(void (^)(id responseObject))success
{
    if ([[url absoluteString] length] == 0)
    {
        CCLog(@"<!> Image Url is NULL");
        return;
    }
    
    if ([[CCCache instance] objectExistAtURL:url])
    {
        UIImage *img = [[CCCache instance] imageForURL:url];
        success(img);
    }
    else
    {
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFHTTPRequestOperation *postOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        postOperation.responseSerializer = [AFImageResponseSerializer serializer];
        [postOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[CCCache instance] saveImage:responseObject forURL:url];
            success(responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            CCLog(@"Image error: %@; url: %@", error, url);
        }];
        [postOperation start];
    }
}

@end
