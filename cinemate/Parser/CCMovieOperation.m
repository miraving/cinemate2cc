//
//  CCMovie.m
//  cinemate
//
//  Created by miraving on 2/11/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCMovieOperation.h"

@implementation CCMovieOperation

- (void)parse
{
    [super parse];
    
    if (self.isCancelled)
        return;
        
    [self parseMovie];
}

- (void)parseMovie
{
    NSDictionary *parameters = [self dataDictionary];
    
    id value = [parameters objectForKey:@"id"];
    
    CCLog(@"> Parse movie id: %@", value);
    
    IfNumberValueNotNull(value)
    {
        CDMovie *movie = [CDMovie movieWithCCID:value];
        
        if (movie == nil)
        {
            movie = [CDMovie insertInManagedObjectContext:[CCDatabaseManager instance].managedObjectContext];
            movie.ccID = value;
        }
        
        value = [parameters objectForKey:@"title_original"];
        IfStringValueNotNull(value)
        {
            movie.title_original = value;
        }
        
        value = [parameters objectForKey:@"title_russian"];
        IfStringValueNotNull(value)
        {
            movie.title_russian = value;
        }
        
        value = [parameters objectForKey:@"title_english"];
        IfStringValueNotNull(value)
        {
            movie.title_english = value;
        }
        
        value = [parameters objectForKey:@"type"];
        IfStringValueNotNull(value)
        {
            movie.type = value;
        }
        
        value = [parameters objectForKey:@"url"];
        IfStringValueNotNull(value)
        {
            movie.url = value;
        }
        
        value = [parameters objectForKey:@"year"];
        IfNumberValueNotNull(value)
        {
            movie.year = [value stringValue];
        }
        
        value = [parameters objectForKey:@"runtime"];
        IfNumberValueNotNull(value)
        {
            movie.runtime = value;
        }
        
        value = [parameters objectForKey:@"description"];
        IfStringValueNotNull(value)
        {
            movie.descriptionMovie = value;
        }
        
        value = [parameters objectForKey:@"release_date_world"];
        IfStringValueNotNull(value)
        {
            NSDateFormatter *formater = [[NSDateFormatter alloc] init];
            [formater setDateFormat:@"yyyy-MM-dd"];
            movie.release_date_world = [formater dateFromString:value];
        }
        
        value = [parameters objectForKey:@"poster"];
        IfDictionaryValueNotNull(value)
        {
            NSDictionary *posterDict = value;
            
            CDPhoto *poster = [CDPhoto photoWithCCID:movie.ccID];
            if (poster == nil)
            {
                poster = [CDPhoto insertInManagedObjectContext:[CCDatabaseManager instance].managedObjectContext];
                poster.ccID = movie.ccID;
            }
            
            poster.moviePoster = movie;

            value = [posterDict valueForKeyPath:@"big.url"];
            IfStringValueNotNull(value)
            {
                poster.big = value;
            }
            
            value = [posterDict valueForKeyPath:@"medium.url"];
            IfStringValueNotNull(value)
            {
                poster.medium = value;
            }
            
            value = [posterDict valueForKeyPath:@"small.url"];
            IfStringValueNotNull(value)
            {
                poster.small = value;
            }
        }
        
        movie.isFullLoad = [NSNumber numberWithBool:YES];
        movie.dateLastLoad = [NSDate date];
    }
    self.complitionBlock();
    [self cancel];
}

@end
