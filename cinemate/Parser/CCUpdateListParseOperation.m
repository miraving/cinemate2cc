//
//  CCUpdateListParseOperation.m
//  cinemate
//
//  Created by miraving on 22.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCUpdateListParseOperation.h"

@implementation CCUpdateListParseOperation

- (void)parse
{
    [super parse];
    
    if (self.isCancelled)
        return;
    
    [self parseUpdateList];
}

- (void)parseUpdateList
{
    NSDictionary *parameters = [self dataDictionary];
    NSString *idDate = [[parameters objectForKey:@"date"] stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    IfStringValueNotNull(idDate)
    {
        CDUpdateList *updateList = [CDUpdateList updateListItemWithDate:idDate];
        
        if (updateList)
        {
            self.complitionBlock();
            [self cancel];
            return;
        }
    
        updateList = [CDUpdateList insertInManagedObjectContext:[CCDatabaseManager instance].managedObjectContext];
        updateList.date = idDate;
        
        id value = [parameters objectForKey:@"url"];
        IfStringValueNotNull(value)
        {
            updateList.url = value;
        }
        
        value = [parameters objectForKey:@"description"];
        IfStringValueNotNull(value)
        {
            updateList.descriptionUpdateList = value;
        }
        
        value = [parameters objectForKey:@"new"];
        IfNumberValueNotNull(value)
        {
            updateList.isNew = value;
        }
        
        value = [parameters valueForKeyPath:@"for_object.movie"];
        IfDictionaryValueNotNull(value)
        {
            id vID = [parameters valueForKeyPath:@"for_object.movie.id"];
            IfNumberValueNotNull(vID)
            {
                CDMovie *movie = [CDMovie movieWithCCID:vID];
                if (!movie)
                {
                    movie = [CDMovie insertInManagedObjectContext:[CCDatabaseManager instance].managedObjectContext];
                    movie.ccID = vID;
                    movie.title_original = [parameters valueForKeyPath:@"for_object.movie.title"];
                }
                updateList.movie = movie;
            }
        }
        
        value = [parameters valueForKeyPath:@"for_object.person"];
        IfDictionaryValueNotNull(value)
        {
            id vID = [parameters valueForKeyPath:@"for_object.person.id"];
            IfNumberValueNotNull(vID)
            {
                CDActor *actor = [CDActor actorWithCCID:vID];
                if (!actor)
                {
                    actor = [CDActor insertInManagedObjectContext:[CCDatabaseManager instance].managedObjectContext];
                    actor.ccID = vID;
                    actor.name_original = [parameters valueForKeyPath:@"for_object.person.title"];
                }
                updateList.actor = actor;
            }
        }
    }
    self.complitionBlock();
    [self cancel];
}

@end
