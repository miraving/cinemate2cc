//
//  CCParserManager.m
//  cinemate
//
//  Created by miraving on 10.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCParserManager.h"
#import "CCModel.h"

#import "CCActorOperation.h"
#import "CCMovieOperation.h"
#import "CCUpdateListParseOperation.h"
#import "CCWatchListParseOperation.h"

@interface CCParserManager ()
@property (nonatomic, retain) NSOperationQueue *operationQueue;
@end

@implementation CCParserManager

+ (instancetype)instance
{
    static CCParserManager *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[CCParserManager alloc] init];
        shared.operationQueue = [[NSOperationQueue alloc] init];
//        [shared.operationQueue setMaxConcurrentOperationCount:1]; // 1
    });
    return shared;
}

#pragma mark - Public API

- (void)insertMovieParseOperationWithParameters:(NSDictionary *)parameters
{
    id result = [parameters objectForKey:@"movie"];
    
    IfArrayValueNotNull(result)
    {
        for (NSDictionary *movieDict in result)
        {
            CCMovieOperation *operation = [[CCMovieOperation alloc] initWithDictionary:movieDict complitionBlock:^{
                NSError *err = nil;
                [[CCDatabaseManager instance] saveDatabase:&err];
                if (err) { CCLog(@"Error: %@", err.localizedDescription); }
            }];
            [self.operationQueue addOperation:operation];
        }
    }
    
    IfDictionaryValueNotNull(result)
    {
        CCMovieOperation *operation = [[CCMovieOperation alloc] initWithDictionary:result complitionBlock:^{
            NSError *err = nil;
            [[CCDatabaseManager instance] saveDatabase:&err];
            if (err) { CCLog(@"Error: %@", err.localizedDescription); }
        }];
        [operation setQueuePriority:NSOperationQueuePriorityLow];
        [self.operationQueue addOperation:operation];
    }
}

- (void)insertActorParseOperationWithParameters:(NSDictionary *)parameters
{
    id result = [parameters objectForKey:@"person"];
    
    IfArrayValueNotNull(result)
    {
        for (NSDictionary *actorDict in result) {
            CCActorOperation *operation = [[CCActorOperation alloc] initWithDictionary:actorDict complitionBlock:^{
                NSError *err = nil;
                [[CCDatabaseManager instance] saveDatabase:&err];
                if (err) { CCLog(@"Error: %@", err.localizedDescription); }
            }];
            [self.operationQueue addOperation:operation];
        }
    }
    
    IfDictionaryValueNotNull(result)
    {
        CCActorOperation *operation = [[CCActorOperation alloc] initWithDictionary:result complitionBlock:^{
            NSError *err = nil;
            [[CCDatabaseManager instance] saveDatabase:&err];
            if (err) { CCLog(@"Error: %@", err.localizedDescription); }
        }];
        [self.operationQueue addOperation:operation];
    }
}

- (void)insertUpdateListParseOperationWithParameters:(NSDictionary *)parameters
{
    id value = [parameters objectForKey:@"item"];
    IfArrayValueNotNull(value)
    {
        for (NSDictionary *dict in value)
        {
            CCUpdateListParseOperation *operation = [[CCUpdateListParseOperation alloc] initWithDictionary:dict complitionBlock:^{
                NSError *err = nil;
                [[CCDatabaseManager instance] saveDatabase:&err];
                if (err) { CCLog(@"Error: %@", err.localizedDescription); }
            }];
            [self.operationQueue addOperation:operation];
        }
    }
    
    IfDictionaryValueNotNull(value)
    {
        CCUpdateListParseOperation *operation = [[CCUpdateListParseOperation alloc] initWithDictionary:value complitionBlock:^{
            NSError *err = nil;
            [[CCDatabaseManager instance] saveDatabase:&err];
            if (err) { CCLog(@"Error: %@", err.localizedDescription); }
        }];
        [self.operationQueue addOperation:operation];
    }
}

- (void)insertWatchListParseOperationWithParameters:(NSDictionary *)parameters
{
    IfDictionaryValueNotNull(parameters)
    {        
        CCWatchListParseOperation *operation = [[CCWatchListParseOperation alloc] initWithDictionary:parameters complitionBlock:^{
            NSError *err = nil;
            [[CCDatabaseManager instance] saveDatabase:&err];
            if (err) { CCLog(@"Error: %@", err.localizedDescription); }
        }];
        [self.operationQueue addOperation:operation];
    }
}

#pragma mark - Privae API

- (NSOperationQueue *)operationQueue
{
    if ([_operationQueue operationCount] <= 1)
    {
        NSError *err = nil;
        [[CCDatabaseManager instance] saveDatabase:&err];
        if (err) { CCLog(@"Error: %@", err.localizedDescription); }
    }
    return _operationQueue;
}


@end







