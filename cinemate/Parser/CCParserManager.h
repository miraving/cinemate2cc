//
//  CCParserManager.h
//  cinemate
//
//  Created by miraving on 10.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCParserManager : NSObject

+ (instancetype)instance;

- (void)insertMovieParseOperationWithParameters:(NSDictionary *)parameters;
- (void)insertActorParseOperationWithParameters:(NSDictionary *)parameters;

- (void)insertUpdateListParseOperationWithParameters:(NSDictionary *)parameters;
- (void)insertWatchListParseOperationWithParameters:(NSDictionary *)parameters;

@end
