//
//  CCParseOperation.m
//  cinemate
//
//  Created by miraving on 2/11/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCParseOperation.h"

@implementation CCParseOperation

- (id)initWithDictionary:(NSDictionary *)dataDictionary complitionBlock:(void (^)())block
{
    self = [self init];
    if (self) {
        self.dataDictionary = dataDictionary;
        self.complitionBlock = block;
    }
    return self;
}

- (void)main
{
    [self parse];
}

- (void)parse
{
    [[CCDatabaseManager instance] resetContext];
}

@end
