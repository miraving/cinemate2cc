//
//  CCParseOperation.h
//  cinemate
//
//  Created by miraving on 2/11/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCParseOperation : NSOperation

@property (nonatomic, copy) void (^complitionBlock)(void);
@property (nonatomic, strong) NSDictionary *dataDictionary;

- (instancetype)initWithDictionary:(NSDictionary *)dataDictionary complitionBlock:(void (^)())block;
- (void)parse;
@end
