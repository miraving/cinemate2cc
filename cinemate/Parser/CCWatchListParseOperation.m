//
//  CCWatchListParseOperation.m
//  cinemate
//
//  Created by miraving on 24.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCWatchListParseOperation.h"

@interface CCWatchListParseOperation ()
{
    int subOperation;
}
@end

@implementation CCWatchListParseOperation

- (void)parse
{
    [super parse];
    
    subOperation = 2;
    
    if (self.isCancelled)
        return;
    
    [self parseWatchList];
}

- (void)cancelOperation
{
    subOperation--;
    if (subOperation == 0)
    {
        [self cancel];
    }
}

- (void)parseWatchList
{
    id value = [self.dataDictionary objectForKey:@"person"];
    IfArrayValueNotNull(value)
    {
        [self parsePerson];
    }
    
    value = [self.dataDictionary objectForKey:@"movie"];
    IfArrayValueNotNull(value)
    {
        [self parseMovie];
    }
    
    self.complitionBlock();
}

#pragma mark - Parse Actor

- (void)parsePerson
{
    NSArray *movieArray = [self.dataDictionary objectForKey:@"person"];
    for (NSDictionary *movieDict in movieArray)
    {
        IfDictionaryValueNotNull(movieDict)
        {
            id value = [movieDict objectForKey:@"id"];
            IfNumberValueNotNull(value)
            {
                CDWatchList *watchList = [CDWatchList watchListItemWithCCID:value];
                if (watchList == nil)
                {
                    watchList = [CDWatchList insertInManagedObjectContext:[CCDatabaseManager instance].managedObjectContext];
                    watchList.ccID = value;
                }
                
                value = [movieDict objectForKey:@"name"];
                IfStringValueNotNull(value)
                {
                    watchList.name = value;
                }
                
                value = [movieDict objectForKey:@"description"];
                IfStringValueNotNull(value)
                {
                    watchList.descriptionWatchList = value;
                }
                
                value = [movieDict objectForKey:@"url"];
                IfStringValueNotNull(value)
                {
                    watchList.url = value;
                    NSNumber *actorCCID = [NSNumber numberWithInt:[[value lastPathComponent] integerValue]];
                    CDActor *actor = [CDActor actorWithCCID:actorCCID];
                    if (actor == nil)
                    {
                        actor = [CDActor insertInManagedObjectContext:[CCDatabaseManager instance].managedObjectContext];
                        actor.ccID = actorCCID;
                        actor.name_original = watchList.name;
                    }
                    [self loadActorWithCCID:actorCCID];
                    watchList.actor = actor;
                }
                
                value = [movieDict objectForKey:@"date"];
                IfStringValueNotNull(value)
                {
                    watchList.date = [value stringByReplacingOccurrencesOfString:@"T" withString:@" "];
                }
            }
        }
    }
    [self cancelOperation];
}

#pragma mark - Parse Movie

- (void)parseMovie
{
    NSArray *movieArray = [self.dataDictionary objectForKey:@"movie"];
    for (NSDictionary *movieDict in movieArray)
    {
        IfDictionaryValueNotNull(movieDict)
        {
            id value = [movieDict objectForKey:@"id"];
            IfNumberValueNotNull(value)
            {
                CDWatchList *watchList = [CDWatchList watchListItemWithCCID:value];
                if (watchList == nil)
                {
                    watchList = [CDWatchList insertInManagedObjectContext:[CCDatabaseManager instance].managedObjectContext];
                    watchList.ccID = value;
                }
                
                value = [movieDict objectForKey:@"name"];
                IfStringValueNotNull(value)
                {
                    watchList.name = value;
                }
                
                value = [movieDict objectForKey:@"description"];
                IfStringValueNotNull(value)
                {
                    watchList.descriptionWatchList = value;
                }
                
                value = [movieDict objectForKey:@"url"];
                IfStringValueNotNull(value)
                {
                    watchList.url = value;
                    
                    NSNumber *movieCCID = [NSNumber numberWithInt:[[value lastPathComponent] integerValue]];
                    CDMovie *movie = [CDMovie movieWithCCID:movieCCID];
                    if (movie == nil)
                    {
                        movie = [CDMovie insertInManagedObjectContext:[CCDatabaseManager instance].managedObjectContext];
                        movie.ccID = movieCCID;
                        movie.title_original = watchList.name;
                    }
                    [self loadMovieWithCCID:movieCCID];
                    watchList.movie = movie;
                }
                
                value = [movieDict objectForKey:@"date"];
                IfStringValueNotNull(value)
                {
                    watchList.date = [value stringByReplacingOccurrencesOfString:@"T" withString:@" "];
                }
            }
        }
    }
    [self cancelOperation];
}

#pragma mark - Load Movie

- (void)loadMovieWithCCID:(NSNumber *)ccID
{
//    Ограничения на сайте
    return;
    
    IfNumberValueNotNull(ccID)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[CCSecurity instance] getMovieAndParseWithCCID:ccID];
        });
    }
}

#pragma mark - Load Actor

- (void)loadActorWithCCID:(NSNumber *)ccID
{
//    Ограничения на сайте
    return;
    
    IfNumberValueNotNull(ccID)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[CCSecurity instance] getActorAndParseWithCCID:ccID];
        });
    }
}

@end


