//
//  CCActor.m
//  cinemate
//
//  Created by miraving on 2/11/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCActorOperation.h"

@implementation CCActorOperation

- (void)parse
{
    [super parse];

    if (self.isCancelled)
        return;
    
    [self parseActor];
}

- (void)parseActor
{
    NSDictionary *parameters = [self dataDictionary];
    
    id value = [parameters objectForKey:@"id"];
    
    CCLog(@"> Parse actor id: %@", value);
    
    IfNumberValueNotNull(value)
    {
        CDActor *actor = [CDActor actorWithCCID:value];
        if (actor == nil)
        {
            actor = [CDActor insertInManagedObjectContext:[CCDatabaseManager instance].managedObjectContext];
            actor.ccID = value;
        }
        
        value = [parameters objectForKey:@"name"];
        IfStringValueNotNull(value)
        {
            actor.name = value;
        }
        
        value = [parameters objectForKey:@"name_original"];
        IfStringValueNotNull(value)
        {
            actor.name_original = value;
        }
        
        value = [parameters objectForKey:@"url"];
        IfStringValueNotNull(value)
        {
            actor.url = value;
        }
        
        value = [parameters valueForKeyPath:@"photo"];
        IfDictionaryValueNotNull(value)
        {
            CDPhoto *photo = [CDPhoto photoWithCCID:actor.ccID];
            if (photo == nil)
            {
                photo = [CDPhoto insertInManagedObjectContext:[CCDatabaseManager instance].managedObjectContext];
                photo.ccID = actor.ccID;
            }
            
            value = [parameters valueForKeyPath:@"photo.small.url"];
            IfStringValueNotNull(value)
            {
                photo.small = value;
            }
            
            value = [parameters valueForKeyPath:@"photo.big.url"];
            IfStringValueNotNull(value)
            {
                photo.big = value;
            }
            
            value = [parameters valueForKeyPath:@"photo.medium.url"];
            IfStringValueNotNull(value)
            {
                photo.medium = value;
            }
            
            photo.actorPhoto = actor;
        }
        
        actor.isFullLoad = [NSNumber numberWithBool:YES];
        actor.dateLastLoad = [NSDate date];
    }
    
    self.complitionBlock();
    [self cancel];
}

@end
