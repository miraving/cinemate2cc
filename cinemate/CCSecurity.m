//
//  CCSecurity.m
//  cinemate
//
//  Created by miraving on 08.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//


#import "CCSecurity.h"


@interface CCSecurity ()
@property (nonatomic, strong) NSString *passKey;
@property (nonatomic, strong) CCUser *userProfile;

@property (nonatomic, strong) NSMutableArray *delegateArray;
@end


@implementation CCSecurity

+ (instancetype)instance
{
    static CCSecurity *instanse = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instanse = [[CCSecurity alloc] init];
        instanse.delegateArray = [NSMutableArray new];
        instanse.passKey = [[NSUserDefaults standardUserDefaults] objectForKey:@"passkey"];
    });
    return instanse;
}

- (void)addDelegate:(id)object
{
    [self.delegateArray addObject:object];
}

- (void)removeDelegate:(id)object
{
    if ([self.delegateArray indexOfObject:object] != NSNotFound)
    {
        [self.delegateArray removeObject:object];
    }
}

- (void)setPassKey:(NSString *)passKey
{
    _passKey = passKey;
    if ([_passKey length] > 0)
    {
        [self saveKey];
        [self loadUserData];
    }
}

#pragma mark - Public API

- (BOOL)auntheticationIsOK
{
    return !(self.passKey == nil);
}

- (void)logout
{
    self.userProfile = nil;
    self.passKey = nil;
    [self saveKey];
}

- (void)loginWithUserName:(NSString *)userLogin andPassword:(NSString *)pass completionBlock:(void (^)(BOOL flag))block
{
    [[CCHTTPClient instance] authenticationUser:userLogin
                                       password:pass
                                     completion:^(NSString *results, NSError *error) {
                                         if (!error && [results length] > 0)
                                         {
                                             self.passKey = results;
                                             block(YES);
                                         }
                                         else
                                         {
                                             self.passKey = nil;
                                             CCLog(@"Error: %@", error.localizedDescription);
                                             block(NO);
                                         }
                                     }];
}

- (void)getUpdateList
{
    if (self.passKey == nil)
        return;
    
    [[CCHTTPClient instance] getUpdateListWithPassKey:self.passKey completion:^(NSDictionary *results, NSError *error) {
        if (!error)
        {
            [[CCParserManager instance] insertUpdateListParseOperationWithParameters:results];
        }
        else
        {
            CCLog(@"Error: %@", error.localizedDescription);
        }
    }];
}

- (void)getWatchList
{
    if (self.passKey == nil)
        return;
    
    [[CCHTTPClient instance] getWatchListWithPassKey:self.passKey completion:^(NSDictionary *results, NSError *error) {
        if (!error)
        {
            [[CCParserManager instance] insertWatchListParseOperationWithParameters:results];
        }
        else
        {
            CCLog(@"Error: %@", error.localizedDescription);
        }
    }];
}

- (void)getMovieAndParseWithCCID:(NSNumber *)ccID
{
    if (self.passKey == nil)
        return;
    
    CDMovie *movie = [CDMovie movieWithCCID:ccID];
    if (movie && [movie.isFullLoad boolValue])
    {
        NSTimeInterval ti = [movie.dateLastLoad timeIntervalSinceDate:[NSDate date]];
        if (ti < 24)
        {
            return;
        }
    }

    [[CCHTTPClient instance] getMovieWithMovieCCID:[ccID stringValue] completion:^(NSDictionary *results, NSError *error)
    {
        if (!error)
        {
            [[CCParserManager instance] insertMovieParseOperationWithParameters:results];
        }
        else
        {
            CCLog(@"Error: %@", error.localizedDescription);
        }
    }];
}

- (void)getActorAndParseWithCCID:(NSNumber *)ccID
{
    if (self.passKey == nil)
        return;

    CDActor *actor = [CDActor actorWithCCID:ccID];
    if (actor && [actor.isFullLoad boolValue])
    {
        NSTimeInterval ti = [actor.dateLastLoad timeIntervalSinceDate:[NSDate date]];
        if (ti < 24)
        {
            return;
        }
    }
    
    [[CCHTTPClient instance] getActorWithActorCCID:[ccID stringValue] completion:^(NSDictionary *results, NSError *error)
    {
        if (!error)
        {
            [[CCParserManager instance] insertActorParseOperationWithParameters:results];
        }
        else
        {
            CCLog(@"Error: %@", error.localizedDescription);
        }
    }];
}

- (void)searchWithTerm:(NSString *)term
{
    if (self.passKey == nil)
        return;
    
//    [[CCHTTPClient instance] searchActorWithTerm:term completion:^(NSDictionary *results, NSError *error)
//    {
//        if (!error)
//        {
//            [[CCParserManager instance] insertActorParseOperationWithParameters:results];
//        }
//        else
//        {
//            CCLog(@"Error: %@", error.localizedDescription);
//        }
//    }];
    
    
    [[CCHTTPClient instance] searchMovieWithTerm:term completion:^(NSDictionary *results, NSError *error)
    {
        if (!error)
        {
            [[CCParserManager instance] insertMovieParseOperationWithParameters:results];
        }
        else
        {
            CCLog(@"Error: %@", error.localizedDescription);
        }
    }];
}

#pragma mark - Private API

- (void)showErrorMessage:(NSString *)message withError:(NSError *)error
{
    NSLog(@"ERROR Message: %@ {%@}", message, error.localizedDescription);
}

- (void)saveKey
{
    if (self.passKey == nil)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"passkey"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:self.passKey forKey:@"passkey"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)loadUserData
{
    [[CCHTTPClient instance] getUserProfileWithPassKey:self.passKey completion:^(NSDictionary *results, NSError *error)
    {
        self.userProfile = [[CCUser alloc] initWithDictionary:results];
        [self sendNotificationWithUserProfile];
    }];
}

- (void)sendNotificationWithUserProfile
{
    for (id delegate in self.delegateArray)
    {
        if ([delegate respondsToSelector:@selector(userProfileWasUpdated:)])
        {
            [delegate userProfileWasUpdated:self.userProfile];
        }
    }
}

@end
