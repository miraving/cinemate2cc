//
//  main.m
//  cinemate
//
//  Created by miraving on 02.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CCAppDelegate class]));
    }
}
