//
//  CCAppDelegate.h
//  cinemate
//
//  Created by miraving on 02.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCMenuViewController.h"
#import "CCSecurity.h"
#import "MFSideMenu.h"

@interface CCAppDelegate : UIResponder <UIApplicationDelegate>


@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MFSideMenuContainerViewController *sideViewController;

+ (id)shared;
- (void)selectSideMenuAtIndex:(SideMenuType)menuType;

@end
