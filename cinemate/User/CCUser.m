//
//  CCUser.m
//  cinemate
//
//  Created by miraving on 09.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCUser.h"

@interface CCUser ()
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSNumber *reputation;
@property (nonatomic, strong) NSURL *bigAvatar;
@property (nonatomic, strong) NSURL *smallAvatar;
@end

@implementation CCUser

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self)
    {
        id value = [dict objectForKey:kUSER_NAME];
        IfStringValueNotNull(value)
        {
            self.userName = value;
        }
        
        value = [dict objectForKey:kUSER_REPUTATION];
        IfNumberValueNotNull(value)
        {
            self.reputation = [NSNumber numberWithInt:(int)value];
        }
        
        value = [dict valueForKeyPath:kUSER_AVATAR_BIG_PATH];
        IfStringValueNotNull(value)
        {
            self.bigAvatar = [NSURL URLWithString:value];
        }
        
        value = [dict valueForKeyPath:kUSER_AVATAR_SMALL_PATH];
        IfStringValueNotNull(value)
        {
            self.smallAvatar = [NSURL URLWithString:value];
        }
    }
    return self;
}


@end
