//
//  CCUser.h
//  cinemate
//
//  Created by miraving on 09.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCUser : NSObject
@property (nonatomic, strong, readonly) NSString *userName;
@property (nonatomic, strong, readonly) NSNumber *reputation;
@property (nonatomic, strong, readonly) NSURL *bigAvatar;
@property (nonatomic, strong, readonly) NSURL *smallAvatar;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
