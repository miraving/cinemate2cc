//
//  CCSettings.h
//  cinemate
//
//  Created by miraving on 02.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//


#define IfStringValueNotNull(p) if (p != nil && p != [NSNull class] && [p isKindOfClass:[NSString class]] && !([p isEqualToString:@""]))

#define IfArrayValueNotNull(p) if (p != nil && p != [NSNull class] && [p isKindOfClass:[NSArray class]] && !([p count] == 0))

#define IfDictionaryValueNotNull(p) if (p != nil && p != [NSNull class] && [p isKindOfClass:[NSDictionary class]] && !([p count] == 0))

#define IfNumberValueNotNull(p) if (p != nil && p != [NSNull class] && [p isKindOfClass:[NSNumber class]])



#define CCRGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define CCRGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]


@interface CCSettings : NSObject
+ (NSDictionary *)navigationBarTextAttributes;
@end
