//
//  UIView+Debug.m
//  cinemate
//
//  Created by miraving on 02.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "UIView+Debug.h"

@implementation UIView (Debug)

- (void)borderColor:(UIColor *)color
{
    self.layer.borderColor = [color CGColor];
    self.layer.borderWidth = 1;
}

- (void)borderRed
{
    [self borderColor:[UIColor redColor]];
}

@end
