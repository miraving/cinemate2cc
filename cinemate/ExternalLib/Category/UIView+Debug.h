//
//  UIView+Debug.h
//  cinemate
//
//  Created by miraving on 02.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Debug)

- (void)borderColor:(UIColor *)color;
- (void)borderRed;

@end
