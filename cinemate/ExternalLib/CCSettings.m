//
//  CCSettings.h
//  cinemate
//
//  Created by miraving on 02.02.14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "CCSettings.h"

@implementation CCSettings

+ (NSDictionary *)navigationBarTextAttributes
{
    return @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:21.0],
             NSForegroundColorAttributeName:[UIColor blueColor]};
}

@end